﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    class clsParámetro
    {
        private Int32 codigoParametro;
        private String nombre;
        private String valor;
        private String descripcion;

        public int CodigoParametro { get => codigoParametro; set => codigoParametro = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Valor { get => valor; set => valor = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
    }
}
