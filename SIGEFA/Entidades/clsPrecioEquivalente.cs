﻿namespace SIGEFA.Entidades
{
	class clsPrecioEquivalente
    {
        public decimal Stock
        { get; set; }

        public decimal Precio
        { get; set; }

        public int Und
        { get; set; }

        public decimal p_compra
        { get; set; }
    }
}
