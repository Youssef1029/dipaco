﻿using DevComponents.DotNetBar.Validator;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
	public partial class frmAgregaInformacionProducto : DevComponents.DotNetBar.Office2007Form
	{

		private Int32 codigoDetPedido, codigoFamilia;
		clsAdmPedido AdmPedido = new clsAdmPedido();
		public Boolean cancelado = false;

		public frmAgregaInformacionProducto(Int32 CodDetallePedido, Int32 CodFamilia)
		{
			codigoDetPedido = CodDetallePedido;
			codigoFamilia = CodFamilia;
			InitializeComponent();
		}

		private void btnGuardar_Click(object sender, EventArgs e)
		{
			try
			{
				/**
				 * Actualizar Detalle Pedido
				 */
				clsDetallePedido detallePedido = new clsDetallePedido
				{
					CodDetallePedido = codigoDetPedido,
					SerieMotor = txtSerie.Text.Trim(),
					NroChasis = txtNChasis.Text.Trim(),
					Modelo = txtModelo.Text.Trim(),
					Marca = txtMarca.Text.Trim(),
					Color = txtColor.Text.Trim()
				};

				if (AdmPedido.updatedetalleadicional(detallePedido))
				{
					MessageBox.Show("Los datos se guardaron correctamente", "Detalle de Pedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
				}
				else
				{
					MessageBox.Show("Ocurrió un problema al realizar la operación", "Detalle de Pedido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message.ToString());
			}
			
		}

		private void txtSerie_Validating(object sender, CancelEventArgs e)
		{
			if (codigoFamilia == 302 || codigoFamilia == 414 || codigoFamilia == 544)
			{

				if (String.IsNullOrEmpty(txtSerie.Text))
				{
					e.Cancel = true;
					errorProvider1.SetError(txtSerie, "Este campo es requerido");
					highlighter1.SetHighlightColor(txtSerie, eHighlightColor.Red);
				}
				else
				{
					errorProvider1.SetError(txtSerie, "");
				}
			}
		}

		private void txtNChasis_Validating(object sender, CancelEventArgs e)
		{
			if (codigoFamilia == 544)
			{
				if (String.IsNullOrEmpty(txtNChasis.Text))
				{
					e.Cancel = true;
					errorProvider1.SetError(txtNChasis, "Este campo es requerido");
					highlighter1.SetHighlightColor(txtNChasis, eHighlightColor.Red);
				}
				else
				{
					errorProvider1.SetError(txtNChasis, "");
				}
			}
		}

		private void txtModelo_Validating(object sender, CancelEventArgs e)
		{
			if (codigoFamilia == 544)
			{
				if (String.IsNullOrEmpty(txtModelo.Text))
				{
					e.Cancel = true;
					errorProvider1.SetError(txtModelo, "Este campo es requerido");
					highlighter1.SetHighlightColor(txtModelo, eHighlightColor.Red);
				}
				else
				{
					errorProvider1.SetError(txtModelo, "");
				}
			}
		}

		private void txtMarca_Validating(object sender, CancelEventArgs e)
		{
			if (codigoFamilia == 544)
			{
				if (String.IsNullOrEmpty(txtMarca.Text))
				{
					e.Cancel = true;
					errorProvider1.SetError(txtMarca, "Este campo es requerido");
					highlighter1.SetHighlightColor(txtMarca, eHighlightColor.Red);
				}
				else
				{
					errorProvider1.SetError(txtMarca, "");
				}
			}
		}

		private void txtColor_Validating(object sender, CancelEventArgs e)
		{
			if (codigoFamilia == 544)
			{
				if (String.IsNullOrEmpty(txtColor.Text))
				{
					e.Cancel = true;
					errorProvider1.SetError(txtColor, "Este campo es requerido");
					highlighter1.SetHighlightColor(txtColor, eHighlightColor.Red);
				}
				else
				{
					errorProvider1.SetError(txtColor, "");
				}
			}
		}

		private void txtSerie_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				btnGuardar.PerformClick();
			}
		}

		private void btnCancelar_Click(object sender, EventArgs e)
		{
			cancelado = true;
			this.Close();
		}

		private void frmAgregaInformacionProducto_Load(object sender, EventArgs e)
		{
			switch (codigoFamilia)
			{
				case 302:
					lblNroChasis.Enabled = false;
					txtNChasis.Enabled = false;
					lblModelo.Enabled = false;
					txtModelo.Enabled = false;
					lblMarca.Enabled = false;
					txtMarca.Enabled = false;
					lblColor.Enabled = false;
					txtColor.Enabled = false;
					txtSerie.Focus();

					break;

				case 414:
					lblNroChasis.Enabled = false;
					txtNChasis.Enabled = false;
					lblModelo.Enabled = false;
					txtModelo.Enabled = false;
					lblMarca.Enabled = false;
					txtMarca.Enabled = false;
					lblColor.Enabled = false;
					txtColor.Enabled = false;
					txtSerie.Focus();

					break;

				case 544:
					/*lblSerie.Enabled = false;
					txtSerie.Enabled = false;*/
					txtSerie.Focus();

					break;
			}
		}
	}
}
