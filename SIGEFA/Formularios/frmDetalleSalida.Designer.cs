﻿namespace SIGEFA.Formularios
{
    partial class frmDetalleSalida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleSalida));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.gbProducto = new System.Windows.Forms.GroupBox();
            this.btnModificarPrecio = new System.Windows.Forms.Button();
            this.txtPrecioNeto = new System.Windows.Forms.TextBox();
            this.txtStockMinimo = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUbicacion = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPrecioDscto = new System.Windows.Forms.TextBox();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.txtDscto2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPrecioNetoDscto = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStock = new System.Windows.Forms.TextBox();
            this.txtUltimoPrecioCompra = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDscto1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbUnidad = new System.Windows.Forms.ComboBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDscto3 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtUnidad = new System.Windows.Forms.TextBox();
            this.txtControlStock = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtUnd = new System.Windows.Forms.TextBox();
            this.txtPrecioNetoDolares = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtPrecioMax = new System.Windows.Forms.TextBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.gbProducto.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // gbProducto
            // 
            this.gbProducto.Controls.Add(this.checkBox2);
            this.gbProducto.Controls.Add(this.checkBox1);
            this.gbProducto.Controls.Add(this.btnModificarPrecio);
            this.gbProducto.Controls.Add(this.txtPrecioNeto);
            this.gbProducto.Controls.Add(this.txtStockMinimo);
            this.gbProducto.Controls.Add(this.label20);
            this.gbProducto.Controls.Add(this.label1);
            this.gbProducto.Controls.Add(this.txtUbicacion);
            this.gbProducto.Controls.Add(this.label15);
            this.gbProducto.Controls.Add(this.txtPrecioDscto);
            this.gbProducto.Controls.Add(this.txtReferencia);
            this.gbProducto.Controls.Add(this.txtDscto2);
            this.gbProducto.Controls.Add(this.label2);
            this.gbProducto.Controls.Add(this.txtPrecioNetoDscto);
            this.gbProducto.Controls.Add(this.txtDescripcion);
            this.gbProducto.Controls.Add(this.label10);
            this.gbProducto.Controls.Add(this.label4);
            this.gbProducto.Controls.Add(this.txtStock);
            this.gbProducto.Controls.Add(this.txtUltimoPrecioCompra);
            this.gbProducto.Controls.Add(this.label5);
            this.gbProducto.Controls.Add(this.label9);
            this.gbProducto.Controls.Add(this.txtCantidad);
            this.gbProducto.Controls.Add(this.label12);
            this.gbProducto.Controls.Add(this.label6);
            this.gbProducto.Controls.Add(this.txtDscto1);
            this.gbProducto.Controls.Add(this.label8);
            this.gbProducto.Controls.Add(this.cmbUnidad);
            this.gbProducto.Controls.Add(this.txtPrecio);
            this.gbProducto.Controls.Add(this.label7);
            this.gbProducto.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProducto.Location = new System.Drawing.Point(12, 3);
            this.gbProducto.Name = "gbProducto";
            this.gbProducto.Size = new System.Drawing.Size(640, 281);
            this.gbProducto.TabIndex = 15;
            this.gbProducto.TabStop = false;
            this.gbProducto.Text = "Ingresar Artículo";
            // 
            // btnModificarPrecio
            // 
            this.btnModificarPrecio.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarPrecio.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarPrecio.Image")));
            this.btnModificarPrecio.Location = new System.Drawing.Point(412, 24);
            this.btnModificarPrecio.Name = "btnModificarPrecio";
            this.btnModificarPrecio.Size = new System.Drawing.Size(205, 26);
            this.btnModificarPrecio.TabIndex = 37;
            this.btnModificarPrecio.Text = "MODIFICAR PRECIO ( CTRL + U)";
            this.btnModificarPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarPrecio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnModificarPrecio.UseVisualStyleBackColor = true;
            this.btnModificarPrecio.Click += new System.EventHandler(this.btnModificarPrecio_Click);
            // 
            // txtPrecioNeto
            // 
            this.txtPrecioNeto.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioNeto.Location = new System.Drawing.Point(337, 173);
            this.txtPrecioNeto.Name = "txtPrecioNeto";
            this.txtPrecioNeto.ReadOnly = true;
            this.txtPrecioNeto.Size = new System.Drawing.Size(161, 25);
            this.txtPrecioNeto.TabIndex = 11;
            this.txtPrecioNeto.TabStop = false;
            this.txtPrecioNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrecioNeto.TextChanged += new System.EventHandler(this.txtPrecioNeto_TextChanged);
            this.txtPrecioNeto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioNeto_KeyPress);
            this.txtPrecioNeto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPrecioNeto_KeyUp);
            this.txtPrecioNeto.Leave += new System.EventHandler(this.txtPrecioNeto_Leave);
            // 
            // txtStockMinimo
            // 
            this.txtStockMinimo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStockMinimo.Location = new System.Drawing.Point(279, 227);
            this.txtStockMinimo.Name = "txtStockMinimo";
            this.txtStockMinimo.ReadOnly = true;
            this.txtStockMinimo.Size = new System.Drawing.Size(120, 25);
            this.txtStockMinimo.TabIndex = 34;
            this.txtStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(276, 207);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 17);
            this.label20.TabIndex = 33;
            this.label20.Text = "Stock Mínimo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Código (F1):";
            // 
            // txtUbicacion
            // 
            this.txtUbicacion.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUbicacion.Location = new System.Drawing.Point(142, 227);
            this.txtUbicacion.Name = "txtUbicacion";
            this.txtUbicacion.ReadOnly = true;
            this.txtUbicacion.Size = new System.Drawing.Size(131, 25);
            this.txtUbicacion.TabIndex = 28;
            this.txtUbicacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(139, 207);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 17);
            this.label15.TabIndex = 27;
            this.label15.Text = "Ubic. en Almacén:";
            // 
            // txtPrecioDscto
            // 
            this.txtPrecioDscto.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioDscto.Location = new System.Drawing.Point(362, 119);
            this.txtPrecioDscto.Name = "txtPrecioDscto";
            this.txtPrecioDscto.ReadOnly = true;
            this.txtPrecioDscto.Size = new System.Drawing.Size(125, 25);
            this.txtPrecioDscto.TabIndex = 21;
            this.txtPrecioDscto.TabStop = false;
            this.txtPrecioDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrecioDscto.Visible = false;
            // 
            // txtReferencia
            // 
            this.txtReferencia.BackColor = System.Drawing.Color.PeachPuff;
            this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReferencia.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReferencia.Location = new System.Drawing.Point(20, 66);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(131, 25);
            this.txtReferencia.TabIndex = 1;
            this.txtReferencia.TextChanged += new System.EventHandler(this.frmDetalleSalida_Load);
            this.txtReferencia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReferencia_KeyDown);
            this.txtReferencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReferencia_KeyPress);
            this.txtReferencia.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtReferencia_KeyUp);
            this.txtReferencia.Leave += new System.EventHandler(this.txtReferencia_Leave);
            // 
            // txtDscto2
            // 
            this.txtDscto2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDscto2.Location = new System.Drawing.Point(230, 173);
            this.txtDscto2.Name = "txtDscto2";
            this.txtDscto2.Size = new System.Drawing.Size(101, 25);
            this.txtDscto2.TabIndex = 9;
            this.txtDscto2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDscto2.Visible = false;
            this.txtDscto2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDscto2_KeyDown);
            this.txtDscto2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto2_KeyPress);
            this.txtDscto2.Leave += new System.EventHandler(this.txtDscto2_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(153, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Descripción :";
            // 
            // txtPrecioNetoDscto
            // 
            this.txtPrecioNetoDscto.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioNetoDscto.Location = new System.Drawing.Point(394, 173);
            this.txtPrecioNetoDscto.Name = "txtPrecioNetoDscto";
            this.txtPrecioNetoDscto.ReadOnly = true;
            this.txtPrecioNetoDscto.Size = new System.Drawing.Size(104, 25);
            this.txtPrecioNetoDscto.TabIndex = 24;
            this.txtPrecioNetoDscto.TabStop = false;
            this.txtPrecioNetoDscto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrecioNetoDscto.Visible = false;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescripcion.Enabled = false;
            this.txtDescripcion.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(157, 66);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.ReadOnly = true;
            this.txtDescripcion.Size = new System.Drawing.Size(460, 25);
            this.txtDescripcion.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(227, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "Aument S./ :";
            this.label10.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(490, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Stock Disponible :";
            // 
            // txtStock
            // 
            this.txtStock.Enabled = false;
            this.txtStock.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStock.Location = new System.Drawing.Point(493, 119);
            this.txtStock.Name = "txtStock";
            this.txtStock.Size = new System.Drawing.Size(124, 25);
            this.txtStock.TabIndex = 3;
            this.txtStock.TextChanged += new System.EventHandler(this.txtStock_TextChanged);
            // 
            // txtUltimoPrecioCompra
            // 
            this.txtUltimoPrecioCompra.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUltimoPrecioCompra.Location = new System.Drawing.Point(20, 227);
            this.txtUltimoPrecioCompra.Name = "txtUltimoPrecioCompra";
            this.txtUltimoPrecioCompra.ReadOnly = true;
            this.txtUltimoPrecioCompra.Size = new System.Drawing.Size(116, 25);
            this.txtUltimoPrecioCompra.TabIndex = 26;
            this.txtUltimoPrecioCompra.TabStop = false;
            this.txtUltimoPrecioCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(359, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Cantidad :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(334, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 17);
            this.label9.TabIndex = 16;
            this.label9.Text = "Precio Total:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCantidad.Location = new System.Drawing.Point(362, 119);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(125, 25);
            this.txtCantidad.TabIndex = 6;
            this.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
            this.txtCantidad.Leave += new System.EventHandler(this.txtCantidad_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 207);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 17);
            this.label12.TabIndex = 22;
            this.label12.Text = "Últim. P. Compra:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Unidad :";
            // 
            // txtDscto1
            // 
            this.txtDscto1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDscto1.Location = new System.Drawing.Point(131, 173);
            this.txtDscto1.MaxLength = 4;
            this.txtDscto1.Name = "txtDscto1";
            this.txtDscto1.Size = new System.Drawing.Size(93, 25);
            this.txtDscto1.TabIndex = 8;
            this.txtDscto1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDscto1.Visible = false;
            this.txtDscto1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDscto1_KeyDown);
            this.txtDscto1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto1_KeyPress);
            this.txtDscto1.Leave += new System.EventHandler(this.txtDscto1_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 153);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Precio Unit:";
            // 
            // cmbUnidad
            // 
            this.cmbUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnidad.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnidad.FormattingEnabled = true;
            this.cmbUnidad.Location = new System.Drawing.Point(20, 119);
            this.cmbUnidad.Name = "cmbUnidad";
            this.cmbUnidad.Size = new System.Drawing.Size(336, 25);
            this.cmbUnidad.TabIndex = 19;
            this.cmbUnidad.SelectionChangeCommitted += new System.EventHandler(this.cmbUnidad_SelectionChangeCommitted);
            // 
            // txtPrecio
            // 
            this.txtPrecio.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecio.Location = new System.Drawing.Point(20, 173);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.ReadOnly = true;
            this.txtPrecio.Size = new System.Drawing.Size(105, 25);
            this.txtPrecio.TabIndex = 7;
            this.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrecio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrecio_KeyDown);
            this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
            this.txtPrecio.Leave += new System.EventHandler(this.txtPrecio_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(128, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = " Dscto S/. :";
            this.label7.Visible = false;
            // 
            // txtDscto3
            // 
            this.txtDscto3.Enabled = false;
            this.txtDscto3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDscto3.Location = new System.Drawing.Point(382, 352);
            this.txtDscto3.Name = "txtDscto3";
            this.txtDscto3.Size = new System.Drawing.Size(95, 25);
            this.txtDscto3.TabIndex = 10;
            this.txtDscto3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDscto3.Visible = false;
            this.txtDscto3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto3_KeyPress);
            this.txtDscto3.Leave += new System.EventHandler(this.txtDscto3_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(302, 355);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 17);
            this.label11.TabIndex = 20;
            this.label11.Text = "% Dscto 3 :";
            this.label11.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(716, 416);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 15);
            this.label14.TabIndex = 27;
            this.label14.Text = "% Max Dscto.";
            this.label14.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(919, 416);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 15);
            this.label13.TabIndex = 23;
            this.label13.Text = "Max Dscto Total:";
            this.label13.Visible = false;
            // 
            // txtUnidad
            // 
            this.txtUnidad.Enabled = false;
            this.txtUnidad.Location = new System.Drawing.Point(710, 435);
            this.txtUnidad.Name = "txtUnidad";
            this.txtUnidad.Size = new System.Drawing.Size(93, 23);
            this.txtUnidad.TabIndex = 4;
            this.txtUnidad.Visible = false;
            // 
            // txtControlStock
            // 
            this.txtControlStock.Location = new System.Drawing.Point(614, 434);
            this.txtControlStock.Name = "txtControlStock";
            this.txtControlStock.Size = new System.Drawing.Size(93, 23);
            this.txtControlStock.TabIndex = 5;
            this.txtControlStock.Visible = false;
            this.txtControlStock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtControlStock_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(611, 416);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Serie / Lote :";
            this.label3.Visible = false;
            // 
            // txtCodigo
            // 
            this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodigo.Location = new System.Drawing.Point(515, 434);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(93, 23);
            this.txtCodigo.TabIndex = 16;
            this.txtCodigo.Visible = false;
            this.txtCodigo.TextChanged += new System.EventHandler(this.txtCodigo_TextChanged);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // txtUnd
            // 
            this.txtUnd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUnd.Location = new System.Drawing.Point(922, 435);
            this.txtUnd.Name = "txtUnd";
            this.txtUnd.Size = new System.Drawing.Size(97, 20);
            this.txtUnd.TabIndex = 20;
            this.txtUnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUnd.Visible = false;
            // 
            // txtPrecioNetoDolares
            // 
            this.txtPrecioNetoDolares.Location = new System.Drawing.Point(810, 435);
            this.txtPrecioNetoDolares.Name = "txtPrecioNetoDolares";
            this.txtPrecioNetoDolares.Size = new System.Drawing.Size(104, 23);
            this.txtPrecioNetoDolares.TabIndex = 21;
            this.txtPrecioNetoDolares.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(327, 435);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(86, 15);
            this.label16.TabIndex = 29;
            this.label16.Text = "Precio Total $/.";
            this.label16.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(407, 235);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(212, 19);
            this.checkBox1.TabIndex = 30;
            this.checkBox1.Text = "AUMENTO SOBRE EL P. UNITARIO";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(407, 210);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(222, 19);
            this.checkBox2.TabIndex = 31;
            this.checkBox2.Text = "DESCUENTO SOBRE EL P. UNITARIO";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // txtPrecioMax
            // 
            this.txtPrecioMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecioMax.Location = new System.Drawing.Point(429, 433);
            this.txtPrecioMax.Name = "txtPrecioMax";
            this.txtPrecioMax.ReadOnly = true;
            this.txtPrecioMax.Size = new System.Drawing.Size(80, 22);
            this.txtPrecioMax.TabIndex = 25;
            this.txtPrecioMax.TabStop = false;
            this.txtPrecioMax.Visible = false;
            // 
            // btnSalir
            // 
            this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(543, 290);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(109, 37);
            this.btnSalir.TabIndex = 18;
            this.btnSalir.Text = "CANCELAR";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Enabled = false;
            this.btnGuardar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(377, 290);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(160, 37);
            this.btnGuardar.TabIndex = 17;
            this.btnGuardar.Text = "AGREGAR A LISTA";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // frmDetalleSalida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnSalir;
            this.ClientSize = new System.Drawing.Size(664, 339);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.gbProducto);
            this.Controls.Add(this.txtDscto3);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtControlStock);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtUnidad);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPrecioMax);
            this.Controls.Add(this.txtUnd);
            this.Controls.Add(this.txtPrecioNetoDolares);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDetalleSalida";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AGREGAR PRODUCTO";
            this.Load += new System.EventHandler(this.frmDetalleSalida_Load);
            this.Shown += new System.EventHandler(this.frmDetalleSalida_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmDetalleSalida_KeyDown);
            this.gbProducto.ResumeLayout(false);
            this.gbProducto.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.GroupBox gbProducto;
        public System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStock;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtCodigo;
        public System.Windows.Forms.TextBox txtDscto3;
        public System.Windows.Forms.TextBox txtDscto2;
        public System.Windows.Forms.TextBox txtPrecioNeto;
        public System.Windows.Forms.TextBox txtDscto1;
        public System.Windows.Forms.TextBox txtPrecio;
        public System.Windows.Forms.TextBox txtCantidad;
        public System.Windows.Forms.TextBox txtControlStock;
        public System.Windows.Forms.TextBox txtUnidad;
        public System.Windows.Forms.TextBox txtPrecioDscto;
        public System.Windows.Forms.TextBox txtUltimoPrecioCompra;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox txtPrecioNetoDscto;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox txtDescripcion;
        public System.Windows.Forms.TextBox txtUnd;
        public System.Windows.Forms.ComboBox cmbUnidad;
        public System.Windows.Forms.TextBox txtPrecioNetoDolares;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.TextBox txtUbicacion;
		private System.Windows.Forms.Label label15;
		public System.Windows.Forms.TextBox txtPrecioMax;
		private System.Windows.Forms.TextBox txtStockMinimo;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button btnModificarPrecio;
	}
}