﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
namespace SIGEFA.Formularios
{
    public partial class frmListaGuiasSinDocumento : Form
    {
        public clsNotaSalida nota = new clsNotaSalida();
        clsAdmNotaSalida AdmNota = new clsAdmNotaSalida();

        public clsFacturaVenta venta = new clsFacturaVenta();
        clsAdmFacturaVenta admVenta = new clsAdmFacturaVenta();
        public int Procede = 0;
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;

        public List<Int32> ltaCod = new List<Int32>();
        public List<clsFacturaVenta> ltaFat = new List<clsFacturaVenta>();
        private Int32 codigo;

        public Int32 CodCliente, Tipo;



        public frmListaGuiasSinDocumento()
        {
            InitializeComponent();
        }

        private void dgvGuias_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvGuias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private void recorrelista()
        {
            ltaCod.Clear();
            if (dgvGuias.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvGuias.SelectedRows)
                {
                    ltaCod.Add(Convert.ToInt32(row.Cells[codnota.Name].Value));
                }
            }
        }

        private void dgvGuias_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
          
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (dgvGuias.SelectedRows != null)
            {
                recorrelista();
                this.Close();
            }
        }

        private void frmListaGuiasSinDocumento_Load(object sender, EventArgs e)
        {
            CargaLista();
        }
        private void CargaLista()
        {

            dgvGuias.DataSource = data;
            data.DataSource = AdmNota.GuiasSinDocumento(frmLogin.iCodAlmacen, CodCliente, Tipo);
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvGuias.ClearSelection();
        }
    }
}
