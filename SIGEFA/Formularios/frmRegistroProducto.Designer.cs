﻿namespace SIGEFA.Formularios
{
    partial class frmRegistroProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistroProducto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnTipoArticulo = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.cbTipoArticulo = new System.Windows.Forms.ComboBox();
            this.linkConfiguraUnidadesEquivalentes = new System.Windows.Forms.LinkLabel();
            this.btnFamilia = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFamilia = new System.Windows.Forms.ComboBox();
            this.btnLinea = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cbLinea = new System.Windows.Forms.ComboBox();
            this.cbMarca = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbGrupo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGrupo = new System.Windows.Forms.Button();
            this.btnMarca = new System.Windows.Forms.Button();
            this.cmbUnidadBase = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnUnidad = new System.Windows.Forms.Button();
            this.gcTipoAfectacion = new DevExpress.XtraEditors.GroupControl();
            this.rdtExonerado = new System.Windows.Forms.RadioButton();
            this.rdtGravado = new System.Windows.Forms.RadioButton();
            this.rdtInafecto = new System.Windows.Forms.RadioButton();
            this.txtStockMinimo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtUbicacion = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCodigoUniversal = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbPrecioVenta = new System.Windows.Forms.Label();
            this.lbLabelCompra = new System.Windows.Forms.Label();
            this.txtPrecioCom = new System.Windows.Forms.TextBox();
            this.txtPrecioVen = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.cbEstado = new System.Windows.Forms.CheckBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodProducto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_procentaje_retencion = new System.Windows.Forms.TextBox();
            this.lb_procentaje_retencion = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtMaxPorcDesc = new System.Windows.Forms.TextBox();
            this.txtPrecioCata = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtComision = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDetraccion = new System.Windows.Forms.CheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cbControlStock = new System.Windows.Forms.ComboBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
            this.requiredFieldValidator1 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.requiredFieldValidator2 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.requiredFieldValidator3 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.requiredFieldValidator4 = new DevComponents.DotNetBar.Validator.RequiredFieldValidator("Your error message here.");
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTipoAfectacion)).BeginInit();
            this.gcTipoAfectacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupControl5);
            this.groupBox1.Controls.Add(this.groupControl4);
            this.groupBox1.Controls.Add(this.groupControl3);
            this.groupBox1.Controls.Add(this.groupControl2);
            this.groupBox1.Controls.Add(this.gcTipoAfectacion);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnGuardar);
            this.groupBox1.Location = new System.Drawing.Point(12, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(627, 534);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // groupControl3
            // 
            this.groupControl3.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl3.AppearanceCaption.Options.UseFont = true;
            this.groupControl3.Controls.Add(this.lbLabelCompra);
            this.groupControl3.Controls.Add(this.txtPrecioCom);
            this.groupControl3.Controls.Add(this.txtPrecioVen);
            this.groupControl3.Controls.Add(this.linkConfiguraUnidadesEquivalentes);
            this.groupControl3.Controls.Add(this.lbPrecioVenta);
            this.groupControl3.Location = new System.Drawing.Point(382, 382);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(232, 85);
            this.groupControl3.TabIndex = 134;
            this.groupControl3.Text = "PRECIOS";
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.btnTipoArticulo);
            this.groupControl2.Controls.Add(this.label36);
            this.groupControl2.Controls.Add(this.cbTipoArticulo);
            this.groupControl2.Controls.Add(this.btnFamilia);
            this.groupControl2.Controls.Add(this.label4);
            this.groupControl2.Controls.Add(this.cbFamilia);
            this.groupControl2.Controls.Add(this.btnLinea);
            this.groupControl2.Controls.Add(this.label5);
            this.groupControl2.Controls.Add(this.cbLinea);
            this.groupControl2.Controls.Add(this.cbMarca);
            this.groupControl2.Controls.Add(this.label6);
            this.groupControl2.Controls.Add(this.cbGrupo);
            this.groupControl2.Controls.Add(this.label10);
            this.groupControl2.Controls.Add(this.btnGrupo);
            this.groupControl2.Controls.Add(this.btnMarca);
            this.groupControl2.Location = new System.Drawing.Point(6, 182);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(608, 194);
            this.groupControl2.TabIndex = 133;
            this.groupControl2.Text = "CLASIFICACIÓN DEL PRODUCTO";
            // 
            // btnTipoArticulo
            // 
            this.btnTipoArticulo.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoArticulo.Image = ((System.Drawing.Image)(resources.GetObject("btnTipoArticulo.Image")));
            this.btnTipoArticulo.Location = new System.Drawing.Point(216, 47);
            this.btnTipoArticulo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnTipoArticulo.Name = "btnTipoArticulo";
            this.btnTipoArticulo.Size = new System.Drawing.Size(90, 26);
            this.btnTipoArticulo.TabIndex = 36;
            this.btnTipoArticulo.Text = "BUSCAR";
            this.btnTipoArticulo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTipoArticulo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTipoArticulo.UseVisualStyleBackColor = true;
            this.btnTipoArticulo.Click += new System.EventHandler(this.btnTipoArticulo_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(14, 32);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(90, 15);
            this.label36.TabIndex = 24;
            this.label36.Text = "Tipo Artículo * :";
            // 
            // cbTipoArticulo
            // 
            this.cbTipoArticulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTipoArticulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoArticulo.FormattingEnabled = true;
            this.cbTipoArticulo.Location = new System.Drawing.Point(17, 51);
            this.cbTipoArticulo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbTipoArticulo.Name = "cbTipoArticulo";
            this.cbTipoArticulo.Size = new System.Drawing.Size(194, 21);
            this.cbTipoArticulo.TabIndex = 4;
            this.cbTipoArticulo.Tag = "1";
            this.superValidator1.SetValidator1(this.cbTipoArticulo, this.requiredFieldValidator4);
            this.cbTipoArticulo.SelectedIndexChanged += new System.EventHandler(this.cbTipoArticulo_SelectedIndexChanged);
            this.cbTipoArticulo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbTipoArticulo_KeyDown);
            // 
            // linkConfiguraUnidadesEquivalentes
            // 
            this.linkConfiguraUnidadesEquivalentes.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkConfiguraUnidadesEquivalentes.Location = new System.Drawing.Point(16, 31);
            this.linkConfiguraUnidadesEquivalentes.Name = "linkConfiguraUnidadesEquivalentes";
            this.linkConfiguraUnidadesEquivalentes.Size = new System.Drawing.Size(203, 42);
            this.linkConfiguraUnidadesEquivalentes.TabIndex = 58;
            this.linkConfiguraUnidadesEquivalentes.TabStop = true;
            this.linkConfiguraUnidadesEquivalentes.Text = "Configurar Unidades Equivalentes y Precios";
            this.linkConfiguraUnidadesEquivalentes.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkConfiguraUnidadesEquivalentes_LinkClicked);
            // 
            // btnFamilia
            // 
            this.btnFamilia.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFamilia.Image = ((System.Drawing.Image)(resources.GetObject("btnFamilia.Image")));
            this.btnFamilia.Location = new System.Drawing.Point(491, 46);
            this.btnFamilia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFamilia.Name = "btnFamilia";
            this.btnFamilia.Size = new System.Drawing.Size(90, 26);
            this.btnFamilia.TabIndex = 37;
            this.btnFamilia.Text = "BUSCAR";
            this.btnFamilia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFamilia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFamilia.UseVisualStyleBackColor = true;
            this.btnFamilia.Click += new System.EventHandler(this.btnFamilia_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(309, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 15);
            this.label4.TabIndex = 16;
            this.label4.Text = "Familia * :";
            // 
            // cbFamilia
            // 
            this.cbFamilia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFamilia.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFamilia.FormattingEnabled = true;
            this.cbFamilia.Location = new System.Drawing.Point(313, 51);
            this.cbFamilia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbFamilia.Name = "cbFamilia";
            this.cbFamilia.Size = new System.Drawing.Size(171, 21);
            this.cbFamilia.TabIndex = 5;
            this.cbFamilia.Tag = "1";
            this.superValidator1.SetValidator1(this.cbFamilia, this.requiredFieldValidator1);
            this.cbFamilia.SelectionChangeCommitted += new System.EventHandler(this.cbFamilia_SelectionChangeCommitted);
            this.cbFamilia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbFamilia_KeyDown);
            this.cbFamilia.Leave += new System.EventHandler(this.cbFamilia_SelectionChangeCommitted);
            // 
            // btnLinea
            // 
            this.btnLinea.Enabled = false;
            this.btnLinea.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLinea.Image = ((System.Drawing.Image)(resources.GetObject("btnLinea.Image")));
            this.btnLinea.Location = new System.Drawing.Point(216, 96);
            this.btnLinea.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLinea.Name = "btnLinea";
            this.btnLinea.Size = new System.Drawing.Size(90, 25);
            this.btnLinea.TabIndex = 38;
            this.btnLinea.Text = "BUSCAR";
            this.btnLinea.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLinea.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLinea.UseVisualStyleBackColor = true;
            this.btnLinea.Click += new System.EventHandler(this.btnLinea_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 18;
            this.label5.Text = "Categoría :";
            // 
            // cbLinea
            // 
            this.cbLinea.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbLinea.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbLinea.Enabled = false;
            this.cbLinea.FormattingEnabled = true;
            this.cbLinea.Location = new System.Drawing.Point(17, 100);
            this.cbLinea.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbLinea.Name = "cbLinea";
            this.cbLinea.Size = new System.Drawing.Size(193, 21);
            this.cbLinea.TabIndex = 6;
            this.cbLinea.SelectionChangeCommitted += new System.EventHandler(this.cbLinea_SelectionChangeCommitted);
            this.cbLinea.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbLinea_KeyDown);
            // 
            // cbMarca
            // 
            this.cbMarca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbMarca.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMarca.FormattingEnabled = true;
            this.cbMarca.Location = new System.Drawing.Point(17, 152);
            this.cbMarca.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbMarca.Name = "cbMarca";
            this.cbMarca.Size = new System.Drawing.Size(193, 21);
            this.cbMarca.TabIndex = 8;
            this.cbMarca.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbMarca_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(312, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 20;
            this.label6.Text = "Modelo :";
            // 
            // cbGrupo
            // 
            this.cbGrupo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbGrupo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbGrupo.Enabled = false;
            this.cbGrupo.FormattingEnabled = true;
            this.cbGrupo.Location = new System.Drawing.Point(314, 100);
            this.cbGrupo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbGrupo.Name = "cbGrupo";
            this.cbGrupo.Size = new System.Drawing.Size(171, 21);
            this.cbGrupo.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 15);
            this.label10.TabIndex = 22;
            this.label10.Text = "Marca :";
            // 
            // btnGrupo
            // 
            this.btnGrupo.Enabled = false;
            this.btnGrupo.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrupo.Image = ((System.Drawing.Image)(resources.GetObject("btnGrupo.Image")));
            this.btnGrupo.Location = new System.Drawing.Point(491, 96);
            this.btnGrupo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGrupo.Name = "btnGrupo";
            this.btnGrupo.Size = new System.Drawing.Size(90, 25);
            this.btnGrupo.TabIndex = 39;
            this.btnGrupo.Text = "BUSCAR";
            this.btnGrupo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGrupo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGrupo.UseVisualStyleBackColor = true;
            this.btnGrupo.Click += new System.EventHandler(this.btnGrupo_Click);
            // 
            // btnMarca
            // 
            this.btnMarca.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarca.Image = ((System.Drawing.Image)(resources.GetObject("btnMarca.Image")));
            this.btnMarca.Location = new System.Drawing.Point(216, 148);
            this.btnMarca.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMarca.Name = "btnMarca";
            this.btnMarca.Size = new System.Drawing.Size(90, 25);
            this.btnMarca.TabIndex = 40;
            this.btnMarca.Text = "BUSCAR";
            this.btnMarca.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMarca.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMarca.UseVisualStyleBackColor = true;
            this.btnMarca.Click += new System.EventHandler(this.btnMarca_Click);
            // 
            // cmbUnidadBase
            // 
            this.cmbUnidadBase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbUnidadBase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbUnidadBase.FormattingEnabled = true;
            this.cmbUnidadBase.Location = new System.Drawing.Point(323, 108);
            this.cmbUnidadBase.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbUnidadBase.Name = "cmbUnidadBase";
            this.cmbUnidadBase.Size = new System.Drawing.Size(160, 21);
            this.cmbUnidadBase.TabIndex = 8;
            this.cmbUnidadBase.Tag = "1";
            this.superValidator1.SetValidator1(this.cmbUnidadBase, this.requiredFieldValidator3);
            this.cmbUnidadBase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUnidadBase_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(321, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 15);
            this.label13.TabIndex = 26;
            this.label13.Text = "Unidad Base * :";
            // 
            // btnUnidad
            // 
            this.btnUnidad.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnidad.Image = ((System.Drawing.Image)(resources.GetObject("btnUnidad.Image")));
            this.btnUnidad.Location = new System.Drawing.Point(489, 105);
            this.btnUnidad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUnidad.Name = "btnUnidad";
            this.btnUnidad.Size = new System.Drawing.Size(91, 25);
            this.btnUnidad.TabIndex = 14;
            this.btnUnidad.Text = "BUSCAR";
            this.btnUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUnidad.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUnidad.UseVisualStyleBackColor = true;
            this.btnUnidad.Click += new System.EventHandler(this.btnUnidad_Click);
            // 
            // gcTipoAfectacion
            // 
            this.gcTipoAfectacion.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gcTipoAfectacion.AppearanceCaption.Options.UseFont = true;
            this.gcTipoAfectacion.Controls.Add(this.rdtExonerado);
            this.gcTipoAfectacion.Controls.Add(this.rdtGravado);
            this.gcTipoAfectacion.Controls.Add(this.rdtInafecto);
            this.gcTipoAfectacion.Location = new System.Drawing.Point(6, 382);
            this.gcTipoAfectacion.Name = "gcTipoAfectacion";
            this.gcTipoAfectacion.Size = new System.Drawing.Size(223, 85);
            this.gcTipoAfectacion.TabIndex = 131;
            this.gcTipoAfectacion.Text = "TIPO DE AFECTACIÓN";
            // 
            // rdtExonerado
            // 
            this.rdtExonerado.AutoSize = true;
            this.rdtExonerado.Location = new System.Drawing.Point(99, 31);
            this.rdtExonerado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdtExonerado.Name = "rdtExonerado";
            this.rdtExonerado.Size = new System.Drawing.Size(87, 17);
            this.rdtExonerado.TabIndex = 122;
            this.rdtExonerado.Text = "EXONERADO";
            this.rdtExonerado.UseVisualStyleBackColor = true;
            this.rdtExonerado.CheckedChanged += new System.EventHandler(this.rdtExonerado_CheckedChanged);
            // 
            // rdtGravado
            // 
            this.rdtGravado.AutoSize = true;
            this.rdtGravado.Checked = true;
            this.rdtGravado.Location = new System.Drawing.Point(20, 31);
            this.rdtGravado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdtGravado.Name = "rdtGravado";
            this.rdtGravado.Size = new System.Drawing.Size(74, 17);
            this.rdtGravado.TabIndex = 120;
            this.rdtGravado.TabStop = true;
            this.rdtGravado.Text = "GRAVADO";
            this.rdtGravado.UseVisualStyleBackColor = true;
            this.rdtGravado.CheckedChanged += new System.EventHandler(this.rdtGravado_CheckedChanged);
            // 
            // rdtInafecto
            // 
            this.rdtInafecto.AutoSize = true;
            this.rdtInafecto.Location = new System.Drawing.Point(20, 56);
            this.rdtInafecto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdtInafecto.Name = "rdtInafecto";
            this.rdtInafecto.Size = new System.Drawing.Size(76, 17);
            this.rdtInafecto.TabIndex = 121;
            this.rdtInafecto.Text = "INAFECTO";
            this.rdtInafecto.UseVisualStyleBackColor = true;
            this.rdtInafecto.CheckedChanged += new System.EventHandler(this.rdtInafecto_CheckedChanged);
            // 
            // txtStockMinimo
            // 
            this.txtStockMinimo.Location = new System.Drawing.Point(21, 48);
            this.txtStockMinimo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStockMinimo.Name = "txtStockMinimo";
            this.txtStockMinimo.Size = new System.Drawing.Size(99, 21);
            this.txtStockMinimo.TabIndex = 12;
            this.txtStockMinimo.Text = "0.00";
            this.txtStockMinimo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtStockMinimo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStockMinimo_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(18, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 15);
            this.label17.TabIndex = 130;
            this.label17.Text = "Stock Mínimo:";
            // 
            // txtUbicacion
            // 
            this.txtUbicacion.Location = new System.Drawing.Point(322, 56);
            this.txtUbicacion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUbicacion.Name = "txtUbicacion";
            this.txtUbicacion.Size = new System.Drawing.Size(259, 21);
            this.txtUbicacion.TabIndex = 129;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(319, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 15);
            this.label16.TabIndex = 128;
            this.label16.Text = "Ubicación en almacén:";
            // 
            // txtCodigoUniversal
            // 
            this.txtCodigoUniversal.Location = new System.Drawing.Point(192, 56);
            this.txtCodigoUniversal.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodigoUniversal.Name = "txtCodigoUniversal";
            this.txtCodigoUniversal.Size = new System.Drawing.Size(124, 21);
            this.txtCodigoUniversal.TabIndex = 127;
            this.txtCodigoUniversal.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(189, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 15);
            this.label11.TabIndex = 126;
            this.label11.Text = "Código Universal:";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // lbPrecioVenta
            // 
            this.lbPrecioVenta.AutoSize = true;
            this.lbPrecioVenta.BackColor = System.Drawing.Color.Transparent;
            this.lbPrecioVenta.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrecioVenta.ForeColor = System.Drawing.Color.Black;
            this.lbPrecioVenta.Location = new System.Drawing.Point(117, 29);
            this.lbPrecioVenta.Name = "lbPrecioVenta";
            this.lbPrecioVenta.Size = new System.Drawing.Size(74, 15);
            this.lbPrecioVenta.TabIndex = 112;
            this.lbPrecioVenta.Text = "P. Venta IGV:";
            // 
            // lbLabelCompra
            // 
            this.lbLabelCompra.AutoSize = true;
            this.lbLabelCompra.BackColor = System.Drawing.Color.Transparent;
            this.lbLabelCompra.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLabelCompra.ForeColor = System.Drawing.Color.Black;
            this.lbLabelCompra.Location = new System.Drawing.Point(12, 29);
            this.lbLabelCompra.Name = "lbLabelCompra";
            this.lbLabelCompra.Size = new System.Drawing.Size(86, 15);
            this.lbLabelCompra.TabIndex = 114;
            this.lbLabelCompra.Text = "P. Compra IGV:";
            // 
            // txtPrecioCom
            // 
            this.txtPrecioCom.BackColor = System.Drawing.Color.White;
            this.txtPrecioCom.ForeColor = System.Drawing.Color.Black;
            this.txtPrecioCom.Location = new System.Drawing.Point(15, 48);
            this.txtPrecioCom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrecioCom.Name = "txtPrecioCom";
            this.txtPrecioCom.Size = new System.Drawing.Size(99, 21);
            this.txtPrecioCom.TabIndex = 10;
            this.txtPrecioCom.Text = "0.00";
            this.txtPrecioCom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioCom.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrecioCom_KeyDown);
            // 
            // txtPrecioVen
            // 
            this.txtPrecioVen.BackColor = System.Drawing.Color.White;
            this.txtPrecioVen.ForeColor = System.Drawing.Color.Black;
            this.txtPrecioVen.Location = new System.Drawing.Point(120, 48);
            this.txtPrecioVen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrecioVen.Name = "txtPrecioVen";
            this.txtPrecioVen.Size = new System.Drawing.Size(99, 21);
            this.txtPrecioVen.TabIndex = 11;
            this.txtPrecioVen.Text = "0.00";
            this.txtPrecioVen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioVen.TextChanged += new System.EventHandler(this.txtPrecioVen_TextChanged);
            this.txtPrecioVen.Leave += new System.EventHandler(this.txtPrecioVen_Leave);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(496, 477);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(118, 41);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(370, 477);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(120, 41);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // cbEstado
            // 
            this.cbEstado.AutoSize = true;
            this.cbEstado.Checked = true;
            this.cbEstado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEstado.Location = new System.Drawing.Point(525, 31);
            this.cbEstado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(56, 17);
            this.cbEstado.TabIndex = 1;
            this.cbEstado.Text = "Activo";
            this.cbEstado.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Location = new System.Drawing.Point(20, 108);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(296, 21);
            this.txtNombre.TabIndex = 3;
            this.txtNombre.Tag = "1";
            this.superValidator1.SetValidator1(this.txtNombre, this.requiredFieldValidator2);
            this.txtNombre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNombre_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nombre * :";
            // 
            // txtReferencia
            // 
            this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtReferencia.Location = new System.Drawing.Point(76, 56);
            this.txtReferencia.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(110, 21);
            this.txtReferencia.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(73, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Código: ";
            // 
            // txtCodProducto
            // 
            this.txtCodProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCodProducto.Enabled = false;
            this.txtCodProducto.Location = new System.Drawing.Point(20, 56);
            this.txtCodProducto.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodProducto.Name = "txtCodProducto";
            this.txtCodProducto.ReadOnly = true;
            this.txtCodProducto.Size = new System.Drawing.Size(50, 21);
            this.txtCodProducto.TabIndex = 0;
            this.txtCodProducto.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Item: ";
            this.label1.Visible = false;
            // 
            // txt_procentaje_retencion
            // 
            this.txt_procentaje_retencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_procentaje_retencion.Location = new System.Drawing.Point(161, 698);
            this.txt_procentaje_retencion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txt_procentaje_retencion.MaxLength = 1000;
            this.txt_procentaje_retencion.Name = "txt_procentaje_retencion";
            this.txt_procentaje_retencion.Size = new System.Drawing.Size(118, 25);
            this.txt_procentaje_retencion.TabIndex = 125;
            this.txt_procentaje_retencion.Text = "0.00";
            this.txt_procentaje_retencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_procentaje_retencion.Visible = false;
            this.txt_procentaje_retencion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_procentaje_retencion_KeyPress);
            this.txt_procentaje_retencion.Leave += new System.EventHandler(this.txt_procentaje_retencion_Leave);
            // 
            // lb_procentaje_retencion
            // 
            this.lb_procentaje_retencion.AutoSize = true;
            this.lb_procentaje_retencion.Location = new System.Drawing.Point(158, 677);
            this.lb_procentaje_retencion.Name = "lb_procentaje_retencion";
            this.lb_procentaje_retencion.Size = new System.Drawing.Size(107, 17);
            this.lb_procentaje_retencion.TabIndex = 124;
            this.lb_procentaje_retencion.Text = "% de Detracción:";
            this.lb_procentaje_retencion.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(450, 688);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 17);
            this.label8.TabIndex = 51;
            this.label8.Text = "%";
            this.label8.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(620, 701);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 17);
            this.label14.TabIndex = 59;
            this.label14.Text = "%";
            this.label14.Visible = false;
            // 
            // txtPeso
            // 
            this.txtPeso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPeso.Location = new System.Drawing.Point(369, 652);
            this.txtPeso.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(97, 25);
            this.txtPeso.TabIndex = 56;
            this.txtPeso.Text = "0.00";
            this.txtPeso.Visible = false;
            this.txtPeso.TextChanged += new System.EventHandler(this.txtPeso_TextChanged);
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(296, 656);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 17);
            this.label12.TabIndex = 57;
            this.label12.Text = "Peso (Kg) :";
            this.label12.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(493, 677);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(127, 17);
            this.label31.TabIndex = 16;
            this.label31.Text = "Máximo Porc. dscto:";
            this.label31.Visible = false;
            // 
            // txtMaxPorcDesc
            // 
            this.txtMaxPorcDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaxPorcDesc.Location = new System.Drawing.Point(496, 698);
            this.txtMaxPorcDesc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMaxPorcDesc.MaxLength = 3;
            this.txtMaxPorcDesc.Name = "txtMaxPorcDesc";
            this.txtMaxPorcDesc.Size = new System.Drawing.Size(118, 25);
            this.txtMaxPorcDesc.TabIndex = 18;
            this.txtMaxPorcDesc.Text = "0.00";
            this.txtMaxPorcDesc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMaxPorcDesc.Visible = false;
            this.txtMaxPorcDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            this.txtMaxPorcDesc.Leave += new System.EventHandler(this.txtMaxPorcDesc_Leave);
            // 
            // txtPrecioCata
            // 
            this.txtPrecioCata.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPrecioCata.Location = new System.Drawing.Point(34, 698);
            this.txtPrecioCata.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrecioCata.MaxLength = 9;
            this.txtPrecioCata.Name = "txtPrecioCata";
            this.txtPrecioCata.Size = new System.Drawing.Size(118, 25);
            this.txtPrecioCata.TabIndex = 17;
            this.txtPrecioCata.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPrecioCata.Visible = false;
            this.txtPrecioCata.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioCata_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 677);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 17);
            this.label9.TabIndex = 52;
            this.label9.Text = "Precio Catálogo :";
            this.label9.Visible = false;
            // 
            // txtComision
            // 
            this.txtComision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComision.Location = new System.Drawing.Point(369, 685);
            this.txtComision.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtComision.Name = "txtComision";
            this.txtComision.Size = new System.Drawing.Size(73, 25);
            this.txtComision.TabIndex = 9;
            this.txtComision.Text = "0.00";
            this.txtComision.Visible = false;
            this.txtComision.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtComision_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(296, 688);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 17);
            this.label7.TabIndex = 49;
            this.label7.Text = "Comisión :";
            this.label7.Visible = false;
            // 
            // cbDetraccion
            // 
            this.cbDetraccion.AutoSize = true;
            this.cbDetraccion.Location = new System.Drawing.Point(299, 715);
            this.cbDetraccion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbDetraccion.Name = "cbDetraccion";
            this.cbDetraccion.Size = new System.Drawing.Size(141, 21);
            this.cbDetraccion.TabIndex = 13;
            this.cbDetraccion.Text = "Afecto a Detraccion";
            this.cbDetraccion.UseVisualStyleBackColor = true;
            this.cbDetraccion.Visible = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(30, 626);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(93, 17);
            this.label38.TabIndex = 35;
            this.label38.Text = "Control Stock :";
            // 
            // cbControlStock
            // 
            this.cbControlStock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbControlStock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbControlStock.DisplayMember = "1,2,3,4";
            this.cbControlStock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbControlStock.FormattingEnabled = true;
            this.cbControlStock.Items.AddRange(new object[] {
            "LIBRE"});
            this.cbControlStock.Location = new System.Drawing.Point(33, 648);
            this.cbControlStock.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbControlStock.Name = "cbControlStock";
            this.cbControlStock.Size = new System.Drawing.Size(212, 25);
            this.cbControlStock.TabIndex = 9;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Write Document.png");
            this.imageList1.Images.SetKeyName(1, "New Document.png");
            this.imageList1.Images.SetKeyName(2, "Remove Document.png");
            this.imageList1.Images.SetKeyName(3, "document-print.png");
            this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
            this.imageList1.Images.SetKeyName(5, "exit.png");
            // 
            // groupControl4
            // 
            this.groupControl4.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl4.AppearanceCaption.Options.UseFont = true;
            this.groupControl4.Controls.Add(this.btnUnidad);
            this.groupControl4.Controls.Add(this.cmbUnidadBase);
            this.groupControl4.Controls.Add(this.txtCodProducto);
            this.groupControl4.Controls.Add(this.label13);
            this.groupControl4.Controls.Add(this.label1);
            this.groupControl4.Controls.Add(this.txtReferencia);
            this.groupControl4.Controls.Add(this.label2);
            this.groupControl4.Controls.Add(this.txtCodigoUniversal);
            this.groupControl4.Controls.Add(this.txtUbicacion);
            this.groupControl4.Controls.Add(this.label16);
            this.groupControl4.Controls.Add(this.txtNombre);
            this.groupControl4.Controls.Add(this.label3);
            this.groupControl4.Controls.Add(this.label11);
            this.groupControl4.Controls.Add(this.cbEstado);
            this.groupControl4.Location = new System.Drawing.Point(6, 25);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(608, 151);
            this.groupControl4.TabIndex = 135;
            this.groupControl4.Text = "DATOS DEL PRODUCTO";
            // 
            // groupControl5
            // 
            this.groupControl5.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl5.AppearanceCaption.Options.UseFont = true;
            this.groupControl5.Controls.Add(this.txtStockMinimo);
            this.groupControl5.Controls.Add(this.label17);
            this.groupControl5.Location = new System.Drawing.Point(235, 382);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(141, 85);
            this.groupControl5.TabIndex = 136;
            this.groupControl5.Text = "CONTROL";
            // 
            // superValidator1
            // 
            this.superValidator1.ContainerControl = this;
            this.superValidator1.ErrorProvider = this.errorProvider1;
            this.superValidator1.Highlighter = this.highlighter1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
            // 
            // highlighter1
            // 
            this.highlighter1.ContainerControl = this;
            // 
            // requiredFieldValidator1
            // 
            this.requiredFieldValidator1.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator1.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator2
            // 
            this.requiredFieldValidator2.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator2.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator3
            // 
            this.requiredFieldValidator3.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator3.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // requiredFieldValidator4
            // 
            this.requiredFieldValidator4.ErrorMessage = "Your error message here.";
            this.requiredFieldValidator4.HighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Red;
            // 
            // frmRegistroProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(651, 542);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_procentaje_retencion);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtPrecioCata);
            this.Controls.Add(this.lb_procentaje_retencion);
            this.Controls.Add(this.cbDetraccion);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtComision);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.txtMaxPorcDesc);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cbControlStock);
            this.Controls.Add(this.label38);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegistroProducto";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registro Producto";
            this.Load += new System.EventHandler(this.frmRegistroProducto_Load);
            this.Shown += new System.EventHandler(this.frmRegistroProducto_Shown);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTipoAfectacion)).EndInit();
            this.gcTipoAfectacion.ResumeLayout(false);
            this.gcTipoAfectacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.CheckBox cbEstado;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodProducto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTipoArticulo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbMarca;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbGrupo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbLinea;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbFamilia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbUnidadBase;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbControlStock;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnUnidad;
        private System.Windows.Forms.Button btnMarca;
        private System.Windows.Forms.Button btnGrupo;
        private System.Windows.Forms.Button btnLinea;
        private System.Windows.Forms.Button btnFamilia;
        private System.Windows.Forms.Button btnTipoArticulo;
        private System.Windows.Forms.CheckBox cbDetraccion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtComision;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPrecioCata;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMaxPorcDesc;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.LinkLabel linkConfiguraUnidadesEquivalentes;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPrecioCom;
        private System.Windows.Forms.Label lbLabelCompra;
        private System.Windows.Forms.TextBox txtPrecioVen;
        private System.Windows.Forms.Label lbPrecioVenta;
        private System.Windows.Forms.RadioButton rdtExonerado;
        private System.Windows.Forms.RadioButton rdtInafecto;
        private System.Windows.Forms.RadioButton rdtGravado;
        private System.Windows.Forms.TextBox txt_procentaje_retencion;
        private System.Windows.Forms.Label lb_procentaje_retencion;
		private System.Windows.Forms.TextBox txtUbicacion;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox txtCodigoUniversal;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtStockMinimo;
		private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.GroupControl gcTipoAfectacion;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator3;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator2;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator4;
        private DevComponents.DotNetBar.Validator.RequiredFieldValidator requiredFieldValidator1;
    }
}