﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.Reportes;
using SIGEFA.Reportes.clsReportes;

namespace SIGEFA.Formularios
{

    public partial class frmVentas : DevComponents.DotNetBar.Office2007Form
    {
        clsAdmNotaSalida AdmNotaS = new clsAdmNotaSalida();
        clsNotaSalida nota = new clsNotaSalida();
        clsNotaIngreso nota2 = new clsNotaIngreso();

        clsTransaccion trans = new clsTransaccion();
        clsSerie ser = new clsSerie();
        clsTipoDocumento doc = new clsTipoDocumento();
        clsAdmTipoDocumento Admdoc = new clsAdmTipoDocumento();
        clsAdmSerie Admser = new clsAdmSerie();
        clsAdmTransaccion admTrans = new clsAdmTransaccion();
        clsAdmNotaIngreso AdmIngreso = new clsAdmNotaIngreso();
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
        clsFacturaVenta venta = new clsFacturaVenta();
        clsPago pag = new clsPago();
        clsAdmPago admPago = new clsAdmPago();
        DataTable dt_AnulaVenta = new DataTable();
        DataTable dt_AnulaPago = new DataTable();
        public Int32 Proceso = 0; //(1)Eliminar (2)Editar (3)Consulta
        List<clsDetalleNotaIngreso> lstNotaIng = new List<clsDetalleNotaIngreso>();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;

        /**
         *
         */
        DataTable listaVentas = new DataTable();
        DataTable dtPagos = new DataTable();
        clsReporteFactura ds = new clsReporteFactura();
        public Byte[] firmadigital { get; set; }
        public Byte[] LogoEmpresa { get; set; }

        public frmVentas()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CargaLista()
        {
            dgvVentas.DataSource = data;
            listaVentas = AdmVenta.Ventas(frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value);
            data.DataSource = listaVentas;
            data.Filter = String.Empty;
            filtro = String.Empty;
            dgvVentas.ClearSelection();

            Double totalVentas = listaVentas.AsEnumerable()
                                            .Sum(x => Convert.ToDouble(x.Field<Object>("total")));

            Double totalVentasActivas = listaVentas.AsEnumerable()
                                           .Where(x => Convert.ToString(x.Field<Object>("anulado").ToString()) == "ACTIVO")
                                           .Sum(x => Convert.ToDouble(x.Field<Object>("total")));

            Double totalVentasAnuladas = listaVentas.AsEnumerable()
                                            .Where(x => Convert.ToString(x.Field<Object>("anulado").ToString()) == "ANULADO")
                                            .Sum(x => Convert.ToDouble(x.Field<Object>("total")));

            lblMontoVentasActivas.Text = "ACTIVAS (S/.): " + totalVentasActivas;
            lblMontoVentasAnuladas.Text = "ANULADAS (S/.): " + totalVentasAnuladas;
            lblMontoTotalVentas.Text = "TOTAL VENTAS (S/.): " + totalVentas;
            lblCantidadVentas.Text = "Nº DE VENTAS: " + dgvVentas.Rows.Count;

            if (dgvVentas.Rows.Count > 0)
            {
                btnReporte.Enabled = true;
            }

        }

        private void btnIrPedido_Click(object sender, EventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow != null)
            {
                DataGridViewRow row = dgvVentas.CurrentRow;
                if (dgvVentas.Rows.Count >= 1)
                {
                    frmVenta form = new frmVenta();
                    form.MdiParent = this.MdiParent;
                    form.CodVenta = venta.CodFacturaVenta;
                    form.Proceso = 3;
                    form.Show();
                }
            }
        }

        private void frmPedidosPendientes_Load(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dgvPedidosPendientes_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && e.Row.Selected)
            {
                venta.CodFacturaVenta = e.Row.Cells[codigo.Name].Value.ToString();
            }
        }

        private void dgvPedidosPendientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && e.RowIndex != -1)
            {
                frmVenta form = new frmVenta();
                form.MdiParent = this.MdiParent;
                form.CodVenta = venta.CodFacturaVenta;
                form.Proceso = 3;
                form.Show();
            }
        }

        private void LeeProductos()
        {
            try
            {
                lstNotaIng.Clear();
                foreach (DataRow row3 in dt_AnulaVenta.Rows)
                {
                    clsDetalleNotaIngreso DetIng = new clsDetalleNotaIngreso();
                    DetIng.CodProducto = Convert.ToInt32(row3[1]);
                    DetIng.CodNotaIngreso = Convert.ToInt32(nota2.CodNotaIngreso);
                    DetIng.CodAlmacen = Convert.ToInt32(row3[3]);
                    DetIng.UnidadIngresada = Convert.ToInt32(row3[4]);
                    DetIng.Cantidad = Convert.ToDouble(row3[6]);
                    DetIng.PrecioUnitario = Convert.ToDouble(row3[29]);
                    DetIng.Descuento1 = Convert.ToDouble(row3[9]);
                    DetIng.Descuento2 = Convert.ToDouble(row3[10]);
                    DetIng.Descuento3 = Convert.ToDouble(row3[11]);
                    DetIng.MontoDescuento = Convert.ToDouble(row3[12]);
                    DetIng.Importe = Convert.ToDouble(row3[29]) * DetIng.Cantidad;
                    DetIng.Subtotal = DetIng.PrecioUnitario * DetIng.Cantidad;
                    DetIng.Igv = DetIng.Importe - DetIng.Subtotal;
                    DetIng.PrecioReal = Convert.ToDouble(row3[29]) * Convert.ToDouble(frmLogin.Configuracion.IGV / 100 + 1);
                    DetIng.ValoReal = Convert.ToDouble(row3[29]);
                    DetIng.CodUser = Convert.ToInt32(row3[17]);
                    DetIng.Estado = true;
                    //DetIng.ValoReal = Convert.ToDouble(row3[29]);
                    lstNotaIng.Add(DetIng);
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void btnAnular_Click(object sender, EventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow.Index != -1)
            {
                DialogResult dlgResult = MessageBox.Show("ESTÁ SEGURO QUE DESEA ANULAR" +
                                                         " EL DOCUMENTO SELECCIONADO",
                                                         "VENTAS", MessageBoxButtons.YesNo,
                                                         MessageBoxIcon.Question);
                if (dlgResult == DialogResult.No)
                {
                    return;
                }
                else
                {
                    venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta));
                    dtPagos = admPago.GetPagosVenta(frmLogin.iCodAlmacen, Convert.ToInt32(venta.CodFacturaVenta));
                    pag = admPago.MuestraPagoVenta(frmLogin.iCodAlmacen, Convert.ToInt32(venta.CodFacturaVenta));

                    /**
                     * Verificar si el documento fue enviado a sunat
                     * 
                     * resultado: verdadero = no enviado
                     *            falso = enviado
                     **/
                    if (AdmVenta.ValidaEnvioSunat(Convert.ToInt32(venta.CodFacturaVenta)))
                    {

                        DialogResult dlgEnviadoSunat = MessageBox.Show("EL DOCUMENTO YA HA SIDO ENVIADO A SUNAT " +
                                                              Environment.NewLine +
                                                              "DESEA ANULAR LA VENTA POR NOTA DE CRÉDITO?",
                                                              "VENTAS", MessageBoxButtons.YesNo,
                                                              MessageBoxIcon.Question);

                        if (dlgEnviadoSunat == DialogResult.No)
                        {
                            return;
                        }
                        else
                        {
                            if (Application.OpenForms["frmNotadeCredito"] != null)
                            {
                                Application.OpenForms["frmNotadeCredito"].Activate();
                            }
                            else
                            {
                                frmNotadeCredito form = new frmNotadeCredito();
                                form.MdiParent = this.MdiParent;
                                form.Dock = DockStyle.Fill;
                                form.Proceso = 7;
                                form.CodNotaS = Convert.ToInt32(dgvVentas.CurrentRow.Cells[codigo.Name].Value);
                                form.Show();
                            }
                        }

                    }
                    else
                    {
                        DialogResult dlgNoEnviado = MessageBox.Show("EL DOCUMENTO NO HA SIDO ENVIADO A SUNAT " +
                                                              Environment.NewLine +
                                                              "Y AL ANULARLO PERDERÁ EL CORRELATIVO - ¿DESEA CONTINUAR?",
                                                              "VENTAS", MessageBoxButtons.YesNo,
                                                              MessageBoxIcon.Question);

                        if (dlgNoEnviado == DialogResult.No)
                        {
                            return;
                        }
                        else
                        {
                            /**
                             * verificar si el documento está anulado
                             */
                            if (!AdmVenta.ValidaAnulacionVenta(Convert.ToInt32(venta.CodFacturaVenta)))
                            {
                                if (AdmVenta.anular(Convert.ToInt32(venta.CodFacturaVenta)))
                                {
                                    MessageBox.Show("DOCUMENTO ANULADO CORRECTAMENTE",
                                                    "VENTAS", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Information);

                                    if (AdmVenta.AnulaRepositorio(Convert.ToInt32(venta.CodFacturaVenta)))
                                    {
                                        MessageBox.Show("DOCUMENTO REMOVIDO DE LA LISTA DE ENVIO A SUNAT",
                                                    "VENTAS", MessageBoxButtons.OK,
                                                    MessageBoxIcon.Information);
                                    }

                                    foreach (DataRow fila in dtPagos.Rows)
                                    {
                                        admPago.AnularPago(Convert.ToInt32(fila[0]));
                                    }

                                    CargaLista();
                                }
                            }
                            else
                            {
                                MessageBox.Show("LA VENTA YA ESTÁ ANULADA",
                                                "VENTAS", MessageBoxButtons.OK,
                                                MessageBoxIcon.Information);
                            }
                        }
                    }
                }
            }
        }

        private void dgvVentas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && e.RowIndex != -1)
            {
                btnIrPedido.Enabled = true;
                if (dgvVentas.Rows[e.RowIndex].Cells[estado.Name].Value.ToString() == "ACTIVO")
                {
                    btnAnular.Enabled = true;
                }
                else
                {
                    btnAnular.Enabled = false;
                }
            }
        }

        private void dtpDesde_ValueChanged(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void dtpHasta_ValueChanged(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("ListaGuias");
            // Columnas
            foreach (DataGridViewColumn column in dgvVentas.Columns)
            {
                DataColumn dc = new DataColumn(column.Name.ToString());
                dt.Columns.Add(dc);
            }
            // Datos
            for (int i = 0; i < dgvVentas.Rows.Count; i++)
            {
                DataGridViewRow row = dgvVentas.Rows[i];
                DataRow dr = dt.NewRow();
                for (int j = 0; j < dgvVentas.Columns.Count; j++)
                {
                    dr[j] = (row.Cells[j].Value == null) ? "" : row.Cells[j].Value.ToString();
                }
                dt.Rows.Add(dr);
            }

            ds.Tables.Add(dt);
            ds.WriteXml("C:\\XML\\ListaVentasRPT.xml", XmlWriteMode.WriteSchema);


            CRListaVentas rpt = new CRListaVentas();
            frmListaVentas frm = new frmListaVentas();
            rpt.SetDataSource(ds);
            frm.crvListaGuias.ReportSource = rpt;
            frm.Show();
        }

        private void dgvVentas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CargaLista();
        }

        private void btnVistaSucursales_Click(object sender, EventArgs e)
        {
            if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow != null)
            {
                if (btnVistaSucursales.Text == "Activar Vista")
                {
                    if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow.Index != -1)
                    {
                        DialogResult dlgResult = MessageBox.Show("¿Esta seguro que desea activar la vista de este documento en otras sucursales?", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dlgResult == DialogResult.No)
                        {
                            return;
                        }
                        else
                        {
                            if (AdmVenta.VistaSucursal(Convert.ToInt32(venta.CodFacturaVenta), 1))
                            {
                                MessageBox.Show("El documento puede ser visualizado desde cualquier sucursal correctamente", "Ventas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CargaLista();
                            }
                        }
                    }
                }
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            DialogResult dlgResult = MessageBox.Show("Esta seguro que desea cambiar venta a pendiente de entrega", "Notas", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlgResult == DialogResult.No)
            {
                return;
            }
            else
            {
                foreach (DataGridViewRow row in dgvVentas.Rows)
                {
                    DataGridViewCheckBoxCell cellSeleccion = row.Cells["pendiente"] as DataGridViewCheckBoxCell;
                    if (Convert.ToBoolean(cellSeleccion.Value))
                    {
                        AdmVenta.VentaPendiente(Convert.ToInt32(row.Cells[codigo.Name].Value));
                    }
                }
            }
        }

        private void lblMontoVentas_Click(object sender, EventArgs e)
        {

        }

        private void vERPDFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvVentas.Rows.Count >= 1 && dgvVentas.CurrentRow != null)
                {
                    DataGridViewRow row = dgvVentas.CurrentRow;
                    if (dgvVentas.Rows.Count >= 1)
                    {

                        venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta));
                        if (venta != null)
                        {

                            if (venta.CodTipoDocumento == 7)
                            {
                                DataSet jes = new DataSet();
                                frmRptFactura frm = new frmRptFactura();
                                CRFacturaXXX rpt = new CRFacturaXXX();

                                rpt.Load("CRFacturaXXX.rpt");
                                jes = ds.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta));
                                foreach (DataTable mel in jes.Tables)
                                {
                                    if (mel.HasErrors)
                                    {
                                        foreach (DataRow changesRow in mel.Rows)
                                        {
                                            if ((int)changesRow["Item", DataRowVersion.Current] > 100)
                                            {
                                                changesRow.RejectChanges();
                                                changesRow.ClearErrors();
                                            }
                                        }
                                    }
                                }
                                rpt.SetDataSource(jes);
                                CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                frm.crvReporteFactura.ReportSource = rpt;
                                frm.ShowDialog();
                                rpt.Close();
                                rpt.Dispose();
                            }
                            else
                            {

                                ser = Admser.MuestraSerie(venta.CodSerie, frmLogin.iCodAlmacen);
                                DataSet jes = new DataSet();

                                frmRptFactura frm = new frmRptFactura();
                                CRReporteFactura rpt = new CRReporteFactura();

                                rpt.Load("CRReporteFactura.rpt");
                                jes = ds.ReporteFactura2(Convert.ToInt32(venta.CodFacturaVenta));

                                String nombrearchivo = "";
                                String rutaLogo = $"{Program.CarpetaLogosEmpresa}\\{frmLogin.iCodEmpresa}.jpg";

                                venta = AdmVenta.BuscaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta), frmLogin.iCodAlmacen);
                                if (venta.CodTipoDocumento == 1)
                                {
                                    nombrearchivo = frmLogin.RUC + "-03-B" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
                                }
                                else if (venta.CodTipoDocumento == 2)
                                {
                                    nombrearchivo = frmLogin.RUC + "-01-F" + venta.Serie + "-" + venta.NumDoc.PadLeft(8, '0');
                                }

                                firmadigital = CargarImagen(@"C:\DOCUMENTOS-" + frmLogin.RUC + "\\CERTIFIK\\QR\\" + nombrearchivo + ".jpeg");
                                LogoEmpresa = CargarImagen(rutaLogo);

                                foreach (DataTable mel in jes.Tables)
                                {
                                    foreach (DataRow changesRow in mel.Rows)
                                    {
                                        changesRow["firma"] = firmadigital;
                                        changesRow["logo_campo"] = LogoEmpresa;
                                    }

                                    if (mel.HasErrors)
                                    {
                                        foreach (DataRow changesRow in mel.Rows)
                                        {
                                            if ((int)changesRow["Item", DataRowVersion.Current] > 100)
                                            {
                                                changesRow.RejectChanges();
                                                changesRow.ClearErrors();
                                            }
                                        }
                                    }
                                }
                                rpt.SetDataSource(jes);
                                CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                                frm.crvReporteFactura.ReportSource = rpt;
                                frm.ShowDialog();
                                rpt.Close();
                                rpt.Dispose();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message,
                                "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public static Byte[] CargarImagen(string rutaArchivo)
        {
            if (rutaArchivo != "")
            {
                try
                {
                    FileStream Archivo = new FileStream(rutaArchivo, FileMode.Open);//Creo el archivo
                    BinaryReader binRead = new BinaryReader(Archivo);//Cargo el Archivo en modo binario
                    Byte[] imagenEnBytes = new Byte[(Int64)Archivo.Length]; //Creo un Array de Bytes donde guardare la imagen
                    binRead.Read(imagenEnBytes, 0, (int)Archivo.Length);//Cargo la imagen en el array de Bytes
                    binRead.Close();
                    Archivo.Close();
                    return imagenEnBytes;//Devuelvo la imagen convertida en un array de bytes
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return new Byte[0];
                }
            }
            return new byte[0];
        }

    }
}
