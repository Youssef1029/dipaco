﻿namespace SIGEFA.Formularios
{
    partial class frmCatalogo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCatalogo));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.ribbonBar2 = new DevComponents.DotNetBar.RibbonBar();
			this.buttonItem16 = new DevComponents.DotNetBar.ButtonItem();
			this.buttonItem6 = new DevComponents.DotNetBar.ButtonItem();
			this.biInhabilitar = new DevComponents.DotNetBar.ButtonItem();
			this.buttonItem2 = new DevComponents.DotNetBar.ButtonItem();
			this.buttonItem3 = new DevComponents.DotNetBar.ButtonItem();
			this.buttonItem4 = new DevComponents.DotNetBar.ButtonItem();
			this.buttonItem5 = new DevComponents.DotNetBar.ButtonItem();
			this.buttonItem9 = new DevComponents.DotNetBar.ButtonItem();
			this.biCatalogo = new DevComponents.DotNetBar.ButtonItem();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblCantidadProductos = new DevComponents.DotNetBar.LabelX();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tpDetalleProducto = new System.Windows.Forms.TabPage();
			this.label8 = new System.Windows.Forms.Label();
			this.txtPrecioCatalogoSoles = new System.Windows.Forms.TextBox();
			this.label39 = new System.Windows.Forms.Label();
			this.txtPrecioCatalogo = new System.Windows.Forms.TextBox();
			this.txtNombre = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtReferencia = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtCodProducto = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tpPrecios = new System.Windows.Forms.TabPage();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codigoundMed = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.unidad1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Factor1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codUndEqui = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.equivalente = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.precios1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tpStockAlmacenes = new System.Windows.Forms.TabPage();
			this.dgvAlmacenes = new System.Windows.Forms.DataGridView();
			this.almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.entregar = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.disponible = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.recibir = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.futuro = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.minimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.maximo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.reposicion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tpProveedores = new System.Windows.Forms.TabPage();
			this.dgvProxProducto = new System.Windows.Forms.DataGridView();
			this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dscto1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dscto2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dscto3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.pneto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.dgvNotas = new System.Windows.Forms.DataGridView();
			this.codnotaproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nota = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dgvProductos = new System.Windows.Forms.DataGridView();
			this.expandablePanel1 = new DevComponents.DotNetBar.ExpandablePanel();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.txtFiltro = new System.Windows.Forms.TextBox();
			this.btnSalir = new System.Windows.Forms.Button();
			this.codproducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.referencia = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
			this.codUniversal = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ubicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.marca = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
			this.unidad = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
			this.control = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.comision = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.preciocatalogo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.precio_compra = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.precio_venta = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.stockminimo = new DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn();
			this.groupBox1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tpDetalleProducto.SuspendLayout();
			this.tpPrecios.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.tpStockAlmacenes.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvAlmacenes)).BeginInit();
			this.tpProveedores.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvProxProducto)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvNotas)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).BeginInit();
			this.expandablePanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "Add Green Button.png");
			this.imageList1.Images.SetKeyName(1, "Add.png");
			this.imageList1.Images.SetKeyName(2, "Remove.png");
			this.imageList1.Images.SetKeyName(3, "Write Document.png");
			this.imageList1.Images.SetKeyName(4, "New Document.png");
			this.imageList1.Images.SetKeyName(5, "Remove Document.png");
			this.imageList1.Images.SetKeyName(6, "1328102023_Copy.png");
			this.imageList1.Images.SetKeyName(7, "document-print.png");
			this.imageList1.Images.SetKeyName(8, "g-icon-new-update.png");
			this.imageList1.Images.SetKeyName(9, "refresh_256.png");
			this.imageList1.Images.SetKeyName(10, "Refresh-icon.png");
			this.imageList1.Images.SetKeyName(11, "search (1).png");
			this.imageList1.Images.SetKeyName(12, "search (5).png");
			this.imageList1.Images.SetKeyName(13, "search (6).png");
			this.imageList1.Images.SetKeyName(14, "search (8).png");
			this.imageList1.Images.SetKeyName(15, "search_top.png");
			this.imageList1.Images.SetKeyName(16, "icon-47203_640.png");
			this.imageList1.Images.SetKeyName(17, "Folder open.png");
			this.imageList1.Images.SetKeyName(18, "sites-icon-large.png");
			this.imageList1.Images.SetKeyName(19, "ksysguard.png");
			// 
			// ribbonBar2
			// 
			this.ribbonBar2.AutoOverflowEnabled = true;
			// 
			// 
			// 
			this.ribbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
			// 
			// 
			// 
			this.ribbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
			this.ribbonBar2.ContainerControlProcessDialogKey = true;
			this.ribbonBar2.Dock = System.Windows.Forms.DockStyle.Top;
			this.ribbonBar2.DragDropSupport = true;
			this.ribbonBar2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ribbonBar2.Images = this.imageList1;
			this.ribbonBar2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem16,
            this.buttonItem6,
            this.biInhabilitar,
            this.buttonItem2,
            this.buttonItem3,
            this.buttonItem4,
            this.buttonItem5,
            this.buttonItem9,
            this.biCatalogo});
			this.ribbonBar2.Location = new System.Drawing.Point(0, 0);
			this.ribbonBar2.Name = "ribbonBar2";
			this.ribbonBar2.Size = new System.Drawing.Size(1236, 64);
			this.ribbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
			this.ribbonBar2.TabIndex = 3;
			this.ribbonBar2.Text = "ribbonBar2";
			// 
			// 
			// 
			this.ribbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
			// 
			// 
			// 
			this.ribbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
			this.ribbonBar2.TitleVisible = false;
			// 
			// buttonItem16
			// 
			this.buttonItem16.ImageIndex = 4;
			this.buttonItem16.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem16.Name = "buttonItem16";
			this.buttonItem16.SubItemsExpandWidth = 14;
			this.buttonItem16.Text = "Nuevo";
			this.buttonItem16.Click += new System.EventHandler(this.buttonItem16_Click);
			// 
			// buttonItem6
			// 
			this.buttonItem6.ImageIndex = 3;
			this.buttonItem6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem6.Name = "buttonItem6";
			this.buttonItem6.SubItemsExpandWidth = 14;
			this.buttonItem6.Text = "Modificar";
			this.buttonItem6.Click += new System.EventHandler(this.buttonItem6_Click);
			// 
			// biInhabilitar
			// 
			this.biInhabilitar.ImageIndex = 5;
			this.biInhabilitar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.biInhabilitar.Name = "biInhabilitar";
			this.biInhabilitar.SubItemsExpandWidth = 14;
			this.biInhabilitar.Text = "Inhabilitar";
			this.biInhabilitar.Click += new System.EventHandler(this.biInhabilitar_Click);
			// 
			// buttonItem2
			// 
			this.buttonItem2.Enabled = false;
			this.buttonItem2.ImageIndex = 19;
			this.buttonItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem2.Name = "buttonItem2";
			this.buttonItem2.SubItemsExpandWidth = 14;
			this.buttonItem2.Text = "Composición Quimica - Dosis";
			this.buttonItem2.Visible = false;
			this.buttonItem2.Click += new System.EventHandler(this.buttonItem2_Click);
			// 
			// buttonItem3
			// 
			this.buttonItem3.Enabled = false;
			this.buttonItem3.ImageIndex = 17;
			this.buttonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem3.Name = "buttonItem3";
			this.buttonItem3.SubItemsExpandWidth = 14;
			this.buttonItem3.Text = "Consultar";
			this.buttonItem3.Visible = false;
			this.buttonItem3.Click += new System.EventHandler(this.buttonItem3_Click);
			// 
			// buttonItem4
			// 
			this.buttonItem4.ImageIndex = 8;
			this.buttonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem4.Name = "buttonItem4";
			this.buttonItem4.SubItemsExpandWidth = 14;
			this.buttonItem4.Text = "Actualizar";
			this.buttonItem4.Click += new System.EventHandler(this.buttonItem4_Click);
			// 
			// buttonItem5
			// 
			this.buttonItem5.ImageIndex = 11;
			this.buttonItem5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem5.Name = "buttonItem5";
			this.buttonItem5.SubItemsExpandWidth = 14;
			this.buttonItem5.Text = "Buscar";
			this.buttonItem5.Click += new System.EventHandler(this.buttonItem5_Click);
			// 
			// buttonItem9
			// 
			this.buttonItem9.ImageIndex = 7;
			this.buttonItem9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.buttonItem9.Name = "buttonItem9";
			this.buttonItem9.SubItemsExpandWidth = 14;
			this.buttonItem9.Text = "Imprimir";
			this.buttonItem9.Click += new System.EventHandler(this.buttonItem9_Click);
			// 
			// biCatalogo
			// 
			this.biCatalogo.Enabled = false;
			this.biCatalogo.ImageIndex = 18;
			this.biCatalogo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
			this.biCatalogo.Name = "biCatalogo";
			this.biCatalogo.SubItemsExpandWidth = 14;
			this.biCatalogo.Text = "Catalogo";
			this.biCatalogo.Visible = false;
			this.biCatalogo.Click += new System.EventHandler(this.biCatalogo_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblCantidadProductos);
			this.groupBox1.Controls.Add(this.tabControl1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(0, 297);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1236, 158);
			this.groupBox1.TabIndex = 13;
			this.groupBox1.TabStop = false;
			// 
			// lblCantidadProductos
			// 
			this.lblCantidadProductos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			// 
			// 
			// 
			this.lblCantidadProductos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
			this.lblCantidadProductos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCantidadProductos.Location = new System.Drawing.Point(936, 18);
			this.lblCantidadProductos.Name = "lblCantidadProductos";
			this.lblCantidadProductos.Size = new System.Drawing.Size(288, 23);
			this.lblCantidadProductos.TabIndex = 5;
			this.lblCantidadProductos.Text = "PRODUCTOS REGISTRADOS : 0";
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tpDetalleProducto);
			this.tabControl1.Controls.Add(this.tpPrecios);
			this.tabControl1.Controls.Add(this.tpStockAlmacenes);
			this.tabControl1.Controls.Add(this.tpProveedores);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(3, 21);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1230, 134);
			this.tabControl1.TabIndex = 4;
			// 
			// tpDetalleProducto
			// 
			this.tpDetalleProducto.Controls.Add(this.label8);
			this.tpDetalleProducto.Controls.Add(this.txtPrecioCatalogoSoles);
			this.tpDetalleProducto.Controls.Add(this.label39);
			this.tpDetalleProducto.Controls.Add(this.txtPrecioCatalogo);
			this.tpDetalleProducto.Controls.Add(this.txtNombre);
			this.tpDetalleProducto.Controls.Add(this.label3);
			this.tpDetalleProducto.Controls.Add(this.txtReferencia);
			this.tpDetalleProducto.Controls.Add(this.label2);
			this.tpDetalleProducto.Controls.Add(this.txtCodProducto);
			this.tpDetalleProducto.Controls.Add(this.label1);
			this.tpDetalleProducto.Location = new System.Drawing.Point(4, 26);
			this.tpDetalleProducto.Name = "tpDetalleProducto";
			this.tpDetalleProducto.Padding = new System.Windows.Forms.Padding(3);
			this.tpDetalleProducto.Size = new System.Drawing.Size(1222, 104);
			this.tpDetalleProducto.TabIndex = 0;
			this.tpDetalleProducto.Text = "Detalle Producto";
			this.tpDetalleProducto.UseVisualStyleBackColor = true;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(535, 53);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(131, 17);
			this.label8.TabIndex = 39;
			this.label8.Text = "Precio Promedio(S/.):";
			// 
			// txtPrecioCatalogoSoles
			// 
			this.txtPrecioCatalogoSoles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecioCatalogoSoles.ForeColor = System.Drawing.Color.SteelBlue;
			this.txtPrecioCatalogoSoles.Location = new System.Drawing.Point(538, 73);
			this.txtPrecioCatalogoSoles.Name = "txtPrecioCatalogoSoles";
			this.txtPrecioCatalogoSoles.ReadOnly = true;
			this.txtPrecioCatalogoSoles.Size = new System.Drawing.Size(128, 20);
			this.txtPrecioCatalogoSoles.TabIndex = 38;
			this.txtPrecioCatalogoSoles.Tag = "1";
			this.txtPrecioCatalogoSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label39
			// 
			this.label39.AutoSize = true;
			this.label39.Location = new System.Drawing.Point(409, 53);
			this.label39.Name = "label39";
			this.label39.Size = new System.Drawing.Size(123, 17);
			this.label39.TabIndex = 37;
			this.label39.Text = "Precio Promedio($):";
			// 
			// txtPrecioCatalogo
			// 
			this.txtPrecioCatalogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPrecioCatalogo.ForeColor = System.Drawing.Color.SteelBlue;
			this.txtPrecioCatalogo.Location = new System.Drawing.Point(412, 73);
			this.txtPrecioCatalogo.Name = "txtPrecioCatalogo";
			this.txtPrecioCatalogo.ReadOnly = true;
			this.txtPrecioCatalogo.Size = new System.Drawing.Size(120, 20);
			this.txtPrecioCatalogo.TabIndex = 36;
			this.txtPrecioCatalogo.Tag = "1";
			this.txtPrecioCatalogo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// txtNombre
			// 
			this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtNombre.ForeColor = System.Drawing.Color.SteelBlue;
			this.txtNombre.Location = new System.Drawing.Point(11, 73);
			this.txtNombre.Name = "txtNombre";
			this.txtNombre.ReadOnly = true;
			this.txtNombre.Size = new System.Drawing.Size(395, 20);
			this.txtNombre.TabIndex = 11;
			this.txtNombre.Tag = "1";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(8, 53);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 17);
			this.label3.TabIndex = 10;
			this.label3.Text = "Nombre :";
			// 
			// txtReferencia
			// 
			this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtReferencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtReferencia.ForeColor = System.Drawing.Color.SteelBlue;
			this.txtReferencia.Location = new System.Drawing.Point(83, 30);
			this.txtReferencia.Name = "txtReferencia";
			this.txtReferencia.ReadOnly = true;
			this.txtReferencia.Size = new System.Drawing.Size(100, 20);
			this.txtReferencia.TabIndex = 9;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(80, 10);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 17);
			this.label2.TabIndex = 8;
			this.label2.Text = "Referencia:";
			// 
			// txtCodProducto
			// 
			this.txtCodProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtCodProducto.ForeColor = System.Drawing.Color.SteelBlue;
			this.txtCodProducto.Location = new System.Drawing.Point(11, 30);
			this.txtCodProducto.Name = "txtCodProducto";
			this.txtCodProducto.ReadOnly = true;
			this.txtCodProducto.Size = new System.Drawing.Size(66, 20);
			this.txtCodProducto.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 10);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 17);
			this.label1.TabIndex = 6;
			this.label1.Text = "Código:";
			// 
			// tpPrecios
			// 
			this.tpPrecios.Controls.Add(this.dataGridView1);
			this.tpPrecios.Location = new System.Drawing.Point(4, 26);
			this.tpPrecios.Name = "tpPrecios";
			this.tpPrecios.Padding = new System.Windows.Forms.Padding(3);
			this.tpPrecios.Size = new System.Drawing.Size(1222, 104);
			this.tpPrecios.TabIndex = 1;
			this.tpPrecios.Text = "Precios";
			this.tpPrecios.UseVisualStyleBackColor = true;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.codigoundMed,
            this.unidad1,
            this.Factor1,
            this.codUndEqui,
            this.equivalente,
            this.precios1,
            this.codTipo,
            this.tipo});
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(3, 3);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridView1.Size = new System.Drawing.Size(1216, 98);
			this.dataGridView1.TabIndex = 12;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.DataPropertyName = "codUnidadEquivalente";
			this.dataGridViewTextBoxColumn3.HeaderText = "codigo";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Visible = false;
			// 
			// codigoundMed
			// 
			this.codigoundMed.DataPropertyName = "codUnidadMedida";
			this.codigoundMed.HeaderText = "codigoundMed";
			this.codigoundMed.Name = "codigoundMed";
			this.codigoundMed.ReadOnly = true;
			this.codigoundMed.Visible = false;
			// 
			// unidad1
			// 
			this.unidad1.DataPropertyName = "descripcion";
			this.unidad1.HeaderText = "Unidad";
			this.unidad1.Name = "unidad1";
			this.unidad1.ReadOnly = true;
			this.unidad1.Width = 150;
			// 
			// Factor1
			// 
			this.Factor1.DataPropertyName = "factor";
			this.Factor1.HeaderText = "Factor";
			this.Factor1.Name = "Factor1";
			this.Factor1.ReadOnly = true;
			this.Factor1.Visible = false;
			// 
			// codUndEqui
			// 
			this.codUndEqui.DataPropertyName = "codUndEqui";
			this.codUndEqui.HeaderText = "codUndEqui";
			this.codUndEqui.Name = "codUndEqui";
			this.codUndEqui.ReadOnly = true;
			this.codUndEqui.Visible = false;
			// 
			// equivalente
			// 
			this.equivalente.DataPropertyName = "equivalente";
			this.equivalente.HeaderText = "Equivalente";
			this.equivalente.Name = "equivalente";
			this.equivalente.ReadOnly = true;
			this.equivalente.Visible = false;
			// 
			// precios1
			// 
			this.precios1.DataPropertyName = "Precio";
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.precios1.DefaultCellStyle = dataGridViewCellStyle1;
			this.precios1.HeaderText = "Precio con IGV";
			this.precios1.Name = "precios1";
			// 
			// codTipo
			// 
			this.codTipo.DataPropertyName = "codTipo";
			this.codTipo.HeaderText = "codTipo";
			this.codTipo.Name = "codTipo";
			this.codTipo.ReadOnly = true;
			this.codTipo.Visible = false;
			// 
			// tipo
			// 
			this.tipo.DataPropertyName = "tip";
			this.tipo.HeaderText = "Tipo Precio";
			this.tipo.Name = "tipo";
			this.tipo.ReadOnly = true;
			this.tipo.Width = 120;
			// 
			// tpStockAlmacenes
			// 
			this.tpStockAlmacenes.Controls.Add(this.dgvAlmacenes);
			this.tpStockAlmacenes.Location = new System.Drawing.Point(4, 26);
			this.tpStockAlmacenes.Name = "tpStockAlmacenes";
			this.tpStockAlmacenes.Size = new System.Drawing.Size(1222, 104);
			this.tpStockAlmacenes.TabIndex = 4;
			this.tpStockAlmacenes.Text = "Stock Almacenes";
			this.tpStockAlmacenes.UseVisualStyleBackColor = true;
			// 
			// dgvAlmacenes
			// 
			this.dgvAlmacenes.AllowUserToAddRows = false;
			this.dgvAlmacenes.AllowUserToDeleteRows = false;
			this.dgvAlmacenes.AllowUserToResizeRows = false;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvAlmacenes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
			this.dgvAlmacenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvAlmacenes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.almacen,
            this.dataGridViewTextBoxColumn2,
            this.entregar,
            this.disponible,
            this.recibir,
            this.futuro,
            this.minimo,
            this.maximo,
            this.reposicion});
			this.dgvAlmacenes.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvAlmacenes.Location = new System.Drawing.Point(0, 0);
			this.dgvAlmacenes.Name = "dgvAlmacenes";
			this.dgvAlmacenes.ReadOnly = true;
			this.dgvAlmacenes.RowHeadersVisible = false;
			this.dgvAlmacenes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvAlmacenes.Size = new System.Drawing.Size(1222, 104);
			this.dgvAlmacenes.TabIndex = 2;
			// 
			// almacen
			// 
			this.almacen.DataPropertyName = "almacen";
			this.almacen.HeaderText = "Almacen";
			this.almacen.Name = "almacen";
			this.almacen.ReadOnly = true;
			this.almacen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.almacen.Width = 200;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.DataPropertyName = "stockactual";
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridViewTextBoxColumn2.HeaderText = "Stock Actual";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.ReadOnly = true;
			this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewTextBoxColumn2.Width = 80;
			// 
			// entregar
			// 
			this.entregar.DataPropertyName = "entregar";
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.entregar.DefaultCellStyle = dataGridViewCellStyle4;
			this.entregar.HeaderText = "P. Entregar";
			this.entregar.Name = "entregar";
			this.entregar.ReadOnly = true;
			this.entregar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.entregar.Visible = false;
			this.entregar.Width = 80;
			// 
			// disponible
			// 
			this.disponible.DataPropertyName = "stockdisponible";
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.disponible.DefaultCellStyle = dataGridViewCellStyle5;
			this.disponible.HeaderText = "Stock Disp.";
			this.disponible.Name = "disponible";
			this.disponible.ReadOnly = true;
			this.disponible.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.disponible.Width = 80;
			// 
			// recibir
			// 
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.recibir.DefaultCellStyle = dataGridViewCellStyle6;
			this.recibir.HeaderText = "P. Recibir";
			this.recibir.Name = "recibir";
			this.recibir.ReadOnly = true;
			this.recibir.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.recibir.Visible = false;
			this.recibir.Width = 80;
			// 
			// futuro
			// 
			dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.futuro.DefaultCellStyle = dataGridViewCellStyle7;
			this.futuro.HeaderText = "Stock Futuro";
			this.futuro.Name = "futuro";
			this.futuro.ReadOnly = true;
			this.futuro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.futuro.Visible = false;
			this.futuro.Width = 80;
			// 
			// minimo
			// 
			this.minimo.DataPropertyName = "stockminimo";
			dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.minimo.DefaultCellStyle = dataGridViewCellStyle8;
			this.minimo.HeaderText = "Stock Minimo";
			this.minimo.Name = "minimo";
			this.minimo.ReadOnly = true;
			this.minimo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			// 
			// maximo
			// 
			this.maximo.DataPropertyName = "stockmaximo";
			dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.maximo.DefaultCellStyle = dataGridViewCellStyle9;
			this.maximo.HeaderText = "Stock Maximo";
			this.maximo.Name = "maximo";
			this.maximo.ReadOnly = true;
			this.maximo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			// 
			// reposicion
			// 
			this.reposicion.DataPropertyName = "stockreposicion";
			dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.reposicion.DefaultCellStyle = dataGridViewCellStyle10;
			this.reposicion.HeaderText = "Stock Reposicion";
			this.reposicion.Name = "reposicion";
			this.reposicion.ReadOnly = true;
			this.reposicion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.reposicion.Visible = false;
			// 
			// tpProveedores
			// 
			this.tpProveedores.Controls.Add(this.dgvProxProducto);
			this.tpProveedores.Location = new System.Drawing.Point(4, 26);
			this.tpProveedores.Name = "tpProveedores";
			this.tpProveedores.Size = new System.Drawing.Size(1222, 104);
			this.tpProveedores.TabIndex = 3;
			this.tpProveedores.Text = "Proveedores";
			this.tpProveedores.UseVisualStyleBackColor = true;
			// 
			// dgvProxProducto
			// 
			this.dgvProxProducto.AllowUserToAddRows = false;
			this.dgvProxProducto.AllowUserToDeleteRows = false;
			this.dgvProxProducto.AllowUserToResizeColumns = false;
			this.dgvProxProducto.AllowUserToResizeRows = false;
			dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvProxProducto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.dgvProxProducto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigo,
            this.dataGridViewTextBoxColumn1,
            this.precio,
            this.dscto1,
            this.dscto2,
            this.dscto3,
            this.pneto});
			this.dgvProxProducto.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvProxProducto.Location = new System.Drawing.Point(0, 0);
			this.dgvProxProducto.Name = "dgvProxProducto";
			this.dgvProxProducto.ReadOnly = true;
			this.dgvProxProducto.RowHeadersVisible = false;
			this.dgvProxProducto.Size = new System.Drawing.Size(1222, 104);
			this.dgvProxProducto.TabIndex = 1;
			// 
			// codigo
			// 
			this.codigo.DataPropertyName = "codProveedor";
			this.codigo.HeaderText = "Cod. Prov.";
			this.codigo.Name = "codigo";
			this.codigo.ReadOnly = true;
			this.codigo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.codigo.Visible = false;
			this.codigo.Width = 60;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.DataPropertyName = "razonsocial";
			this.dataGridViewTextBoxColumn1.HeaderText = "Proveedor";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.ReadOnly = true;
			this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewTextBoxColumn1.Width = 200;
			// 
			// precio
			// 
			this.precio.DataPropertyName = "precioofrecido";
			dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle12.Format = "N2";
			this.precio.DefaultCellStyle = dataGridViewCellStyle12;
			this.precio.HeaderText = "Precio";
			this.precio.Name = "precio";
			this.precio.ReadOnly = true;
			this.precio.Width = 65;
			// 
			// dscto1
			// 
			this.dscto1.DataPropertyName = "dscto1";
			dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle13.Format = "N2";
			this.dscto1.DefaultCellStyle = dataGridViewCellStyle13;
			this.dscto1.HeaderText = "Dscto. 1";
			this.dscto1.Name = "dscto1";
			this.dscto1.ReadOnly = true;
			this.dscto1.Visible = false;
			this.dscto1.Width = 60;
			// 
			// dscto2
			// 
			this.dscto2.DataPropertyName = "dscto2";
			dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle14.Format = "N2";
			this.dscto2.DefaultCellStyle = dataGridViewCellStyle14;
			this.dscto2.HeaderText = "Dscto. 2";
			this.dscto2.Name = "dscto2";
			this.dscto2.ReadOnly = true;
			this.dscto2.Visible = false;
			this.dscto2.Width = 60;
			// 
			// dscto3
			// 
			this.dscto3.DataPropertyName = "dscto3";
			dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle15.Format = "N2";
			this.dscto3.DefaultCellStyle = dataGridViewCellStyle15;
			this.dscto3.HeaderText = "Dscto. 3";
			this.dscto3.Name = "dscto3";
			this.dscto3.ReadOnly = true;
			this.dscto3.Visible = false;
			this.dscto3.Width = 60;
			// 
			// pneto
			// 
			this.pneto.DataPropertyName = "precio";
			dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle16.Format = "N2";
			this.pneto.DefaultCellStyle = dataGridViewCellStyle16;
			this.pneto.HeaderText = "P. Neto";
			this.pneto.Name = "pneto";
			this.pneto.ReadOnly = true;
			this.pneto.Width = 65;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.dgvNotas);
			this.tabPage3.Location = new System.Drawing.Point(4, 26);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(1222, 104);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Notas";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// dgvNotas
			// 
			this.dgvNotas.AllowUserToAddRows = false;
			this.dgvNotas.AllowUserToDeleteRows = false;
			this.dgvNotas.AllowUserToResizeColumns = false;
			this.dgvNotas.AllowUserToResizeRows = false;
			this.dgvNotas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvNotas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codnotaproducto,
            this.usuario,
            this.nota});
			this.dgvNotas.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvNotas.Enabled = false;
			this.dgvNotas.Location = new System.Drawing.Point(3, 3);
			this.dgvNotas.Name = "dgvNotas";
			this.dgvNotas.ReadOnly = true;
			this.dgvNotas.RowHeadersVisible = false;
			this.dgvNotas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvNotas.Size = new System.Drawing.Size(1216, 98);
			this.dgvNotas.TabIndex = 5;
			// 
			// codnotaproducto
			// 
			this.codnotaproducto.DataPropertyName = "codNota";
			this.codnotaproducto.HeaderText = "Codigo";
			this.codnotaproducto.Name = "codnotaproducto";
			this.codnotaproducto.ReadOnly = true;
			this.codnotaproducto.Visible = false;
			// 
			// usuario
			// 
			this.usuario.DataPropertyName = "usuario";
			this.usuario.HeaderText = "Usuario";
			this.usuario.Name = "usuario";
			this.usuario.ReadOnly = true;
			this.usuario.Width = 200;
			// 
			// nota
			// 
			this.nota.DataPropertyName = "nota";
			this.nota.HeaderText = "Nota";
			this.nota.Name = "nota";
			this.nota.ReadOnly = true;
			this.nota.Width = 600;
			// 
			// dgvProductos
			// 
			this.dgvProductos.AllowUserToAddRows = false;
			this.dgvProductos.AllowUserToDeleteRows = false;
			this.dgvProductos.AllowUserToResizeColumns = false;
			this.dgvProductos.AllowUserToResizeRows = false;
			this.dgvProductos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
			dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvProductos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
			this.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codproducto,
            this.referencia,
            this.codUniversal,
            this.ubicacion,
            this.nombre,
            this.modelo,
            this.marca,
            this.unidad,
            this.control,
            this.comision,
            this.preciocatalogo,
            this.precio_compra,
            this.precio_venta,
            this.stockminimo});
			dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dgvProductos.DefaultCellStyle = dataGridViewCellStyle19;
			this.dgvProductos.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvProductos.Location = new System.Drawing.Point(0, 64);
			this.dgvProductos.Name = "dgvProductos";
			this.dgvProductos.ReadOnly = true;
			this.dgvProductos.RowHeadersVisible = false;
			this.dgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvProductos.Size = new System.Drawing.Size(1236, 233);
			this.dgvProductos.TabIndex = 14;
			this.dgvProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductos_CellClick);
			this.dgvProductos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductos_CellDoubleClick);
			this.dgvProductos.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProductos_ColumnHeaderMouseClick);
			this.dgvProductos.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvProductos_RowStateChanged);
			// 
			// expandablePanel1
			// 
			this.expandablePanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.expandablePanel1.AnimationTime = 200;
			this.expandablePanel1.CanvasColor = System.Drawing.SystemColors.GradientActiveCaption;
			this.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
			this.expandablePanel1.Controls.Add(this.label4);
			this.expandablePanel1.Controls.Add(this.label5);
			this.expandablePanel1.Controls.Add(this.label6);
			this.expandablePanel1.Controls.Add(this.label7);
			this.expandablePanel1.Controls.Add(this.txtFiltro);
			this.expandablePanel1.Controls.Add(this.btnSalir);
			this.expandablePanel1.DisabledBackColor = System.Drawing.Color.Empty;
			this.expandablePanel1.ExpandButtonVisible = false;
			this.expandablePanel1.Expanded = false;
			this.expandablePanel1.ExpandedBounds = new System.Drawing.Rectangle(603, 0, 297, 93);
			this.expandablePanel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.expandablePanel1.Location = new System.Drawing.Point(938, 0);
			this.expandablePanel1.Name = "expandablePanel1";
			this.expandablePanel1.ShowFocusRectangle = true;
			this.expandablePanel1.Size = new System.Drawing.Size(297, 0);
			this.expandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center;
			this.expandablePanel1.Style.BackColor1.Color = System.Drawing.SystemColors.GradientActiveCaption;
			this.expandablePanel1.Style.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption;
			this.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
			this.expandablePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarPopupBorder;
			this.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
			this.expandablePanel1.Style.GradientAngle = 90;
			this.expandablePanel1.TabIndex = 18;
			this.expandablePanel1.TitleHeight = 0;
			this.expandablePanel1.TitleStyle.Alignment = System.Drawing.StringAlignment.Center;
			this.expandablePanel1.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
			this.expandablePanel1.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
			this.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner;
			this.expandablePanel1.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
			this.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
			this.expandablePanel1.TitleStyle.GradientAngle = 90;
			this.expandablePanel1.TitleText = "Title Bar";
			this.expandablePanel1.Visible = false;
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.BackColor = System.Drawing.Color.Transparent;
			this.label4.Location = new System.Drawing.Point(12, -58);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(31, 15);
			this.label4.TabIndex = 10;
			this.label4.Text = "Por :";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.BackColor = System.Drawing.Color.Transparent;
			this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(13, -83);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(68, 17);
			this.label5.TabIndex = 9;
			this.label5.Text = "Búsqueda";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.BackColor = System.Drawing.Color.Transparent;
			this.label6.ForeColor = System.Drawing.Color.LightBlue;
			this.label6.Location = new System.Drawing.Point(273, -58);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(12, 15);
			this.label6.TabIndex = 7;
			this.label6.Text = "x";
			this.label6.Visible = false;
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.BackColor = System.Drawing.Color.Transparent;
			this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(49, -57);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(14, 13);
			this.label7.TabIndex = 6;
			this.label7.Text = "X";
			// 
			// txtFiltro
			// 
			this.txtFiltro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.txtFiltro.Location = new System.Drawing.Point(15, -39);
			this.txtFiltro.Name = "txtFiltro";
			this.txtFiltro.Size = new System.Drawing.Size(270, 23);
			this.txtFiltro.TabIndex = 5;
			this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
			this.txtFiltro.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFiltro_KeyDown);
			// 
			// btnSalir
			// 
			this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSalir.BackColor = System.Drawing.Color.Transparent;
			this.btnSalir.FlatAppearance.BorderSize = 0;
			this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
			this.btnSalir.Location = new System.Drawing.Point(267, -89);
			this.btnSalir.Margin = new System.Windows.Forms.Padding(1);
			this.btnSalir.Name = "btnSalir";
			this.btnSalir.Size = new System.Drawing.Size(18, 22);
			this.btnSalir.TabIndex = 3;
			this.btnSalir.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			this.btnSalir.UseVisualStyleBackColor = false;
			this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
			// 
			// codproducto
			// 
			this.codproducto.DataPropertyName = "codProducto";
			this.codproducto.HeaderText = "Código_";
			this.codproducto.Name = "codproducto";
			this.codproducto.ReadOnly = true;
			this.codproducto.Visible = false;
			this.codproducto.Width = 90;
			// 
			// referencia
			// 
			this.referencia.DataPropertyName = "referencia";
			this.referencia.HeaderText = "Código";
			this.referencia.Name = "referencia";
			this.referencia.ReadOnly = true;
			this.referencia.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.referencia.Width = 90;
			// 
			// codUniversal
			// 
			this.codUniversal.DataPropertyName = "codUniversal";
			this.codUniversal.HeaderText = "Código Universal";
			this.codUniversal.Name = "codUniversal";
			this.codUniversal.ReadOnly = true;
			this.codUniversal.Visible = false;
			this.codUniversal.Width = 130;
			// 
			// ubicacion
			// 
			this.ubicacion.DataPropertyName = "ubicacion";
			this.ubicacion.HeaderText = "Ubicación";
			this.ubicacion.Name = "ubicacion";
			this.ubicacion.ReadOnly = true;
			this.ubicacion.Visible = false;
			// 
			// nombre
			// 
			this.nombre.DataPropertyName = "descripcion";
			this.nombre.HeaderText = "Nombre";
			this.nombre.Name = "nombre";
			this.nombre.ReadOnly = true;
			this.nombre.Width = 400;
			// 
			// modelo
			// 
			this.modelo.DataPropertyName = "modelo";
			this.modelo.HeaderText = "Modelo";
			this.modelo.Name = "modelo";
			this.modelo.ReadOnly = true;
			// 
			// marca
			// 
			this.marca.DataPropertyName = "nmarca";
			this.marca.HeaderText = "Marca";
			this.marca.Name = "marca";
			this.marca.ReadOnly = true;
			this.marca.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.marca.Width = 180;
			// 
			// unidad
			// 
			this.unidad.DataPropertyName = "unidad";
			this.unidad.HeaderText = "Unidad";
			this.unidad.Name = "unidad";
			this.unidad.ReadOnly = true;
			this.unidad.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.unidad.Width = 130;
			// 
			// control
			// 
			this.control.DataPropertyName = "control";
			dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			dataGridViewCellStyle18.Format = "N2";
			this.control.DefaultCellStyle = dataGridViewCellStyle18;
			this.control.HeaderText = "Control Stock";
			this.control.Name = "control";
			this.control.ReadOnly = true;
			this.control.Visible = false;
			// 
			// comision
			// 
			this.comision.DataPropertyName = "comision";
			this.comision.HeaderText = "Comision";
			this.comision.Name = "comision";
			this.comision.ReadOnly = true;
			this.comision.Visible = false;
			// 
			// preciocatalogo
			// 
			this.preciocatalogo.DataPropertyName = "preciocatalogo";
			this.preciocatalogo.HeaderText = "P. Catálogo";
			this.preciocatalogo.Name = "preciocatalogo";
			this.preciocatalogo.ReadOnly = true;
			this.preciocatalogo.Visible = false;
			// 
			// precio_compra
			// 
			this.precio_compra.DataPropertyName = "precio_compra";
			this.precio_compra.HeaderText = "P. Compra (U. Base)";
			this.precio_compra.Name = "precio_compra";
			this.precio_compra.ReadOnly = true;
			// 
			// precio_venta
			// 
			this.precio_venta.DataPropertyName = "precio_venta";
			this.precio_venta.HeaderText = "P. Venta (U. Base)";
			this.precio_venta.Name = "precio_venta";
			this.precio_venta.ReadOnly = true;
			// 
			// stockminimo
			// 
			this.stockminimo.DataPropertyName = "stockminimo";
			this.stockminimo.HeaderText = "Stock Mínimo";
			this.stockminimo.Name = "stockminimo";
			this.stockminimo.ReadOnly = true;
			this.stockminimo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			// 
			// frmCatalogo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1236, 455);
			this.Controls.Add(this.expandablePanel1);
			this.Controls.Add(this.dgvProductos);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.ribbonBar2);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Name = "frmCatalogo";
			this.Text = "Catalogo de Productos";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmProductos_Load);
			this.Shown += new System.EventHandler(this.frmProductos_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmProductos_KeyDown);
			this.groupBox1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tpDetalleProducto.ResumeLayout(false);
			this.tpDetalleProducto.PerformLayout();
			this.tpPrecios.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.tpStockAlmacenes.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvAlmacenes)).EndInit();
			this.tpProveedores.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvProxProducto)).EndInit();
			this.tabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvNotas)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).EndInit();
			this.expandablePanel1.ResumeLayout(false);
			this.expandablePanel1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private DevComponents.DotNetBar.RibbonBar ribbonBar2;
        private DevComponents.DotNetBar.ButtonItem buttonItem16;
        private DevComponents.DotNetBar.ButtonItem buttonItem6;
        private DevComponents.DotNetBar.ButtonItem biInhabilitar;
        private DevComponents.DotNetBar.ButtonItem buttonItem3;
        private DevComponents.DotNetBar.ButtonItem buttonItem4;
        private DevComponents.DotNetBar.ButtonItem buttonItem5;
        private DevComponents.DotNetBar.ButtonItem buttonItem9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvProductos;
        private DevComponents.DotNetBar.ExpandablePanel expandablePanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Button btnSalir;
        private DevComponents.DotNetBar.ButtonItem biCatalogo;
        private DevComponents.DotNetBar.ButtonItem buttonItem2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpDetalleProducto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPrecioCatalogoSoles;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtPrecioCatalogo;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodProducto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tpPrecios;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoundMed;
        private System.Windows.Forms.DataGridViewTextBoxColumn unidad1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factor1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codUndEqui;
        private System.Windows.Forms.DataGridViewTextBoxColumn equivalente;
        private System.Windows.Forms.DataGridViewTextBoxColumn precios1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipo;
        private System.Windows.Forms.TabPage tpStockAlmacenes;
        private System.Windows.Forms.DataGridView dgvAlmacenes;
        private System.Windows.Forms.DataGridViewTextBoxColumn almacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn entregar;
        private System.Windows.Forms.DataGridViewTextBoxColumn disponible;
        private System.Windows.Forms.DataGridViewTextBoxColumn recibir;
        private System.Windows.Forms.DataGridViewTextBoxColumn futuro;
        private System.Windows.Forms.DataGridViewTextBoxColumn minimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn maximo;
        private System.Windows.Forms.DataGridViewTextBoxColumn reposicion;
        private System.Windows.Forms.TabPage tpProveedores;
        public System.Windows.Forms.DataGridView dgvProxProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscto3;
        private System.Windows.Forms.DataGridViewTextBoxColumn pneto;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dgvNotas;
        private System.Windows.Forms.DataGridViewTextBoxColumn codnotaproducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn nota;
		private DevComponents.DotNetBar.LabelX lblCantidadProductos;
		private System.Windows.Forms.DataGridViewTextBoxColumn codproducto;
		private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn referencia;
		private System.Windows.Forms.DataGridViewTextBoxColumn codUniversal;
		private System.Windows.Forms.DataGridViewTextBoxColumn ubicacion;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
		private System.Windows.Forms.DataGridViewTextBoxColumn modelo;
		private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn marca;
		private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn unidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn control;
		private System.Windows.Forms.DataGridViewTextBoxColumn comision;
		private System.Windows.Forms.DataGridViewTextBoxColumn preciocatalogo;
		private System.Windows.Forms.DataGridViewTextBoxColumn precio_compra;
		private System.Windows.Forms.DataGridViewTextBoxColumn precio_venta;
		private DataGridViewAutoFilter.DataGridViewAutoFilterTextBoxColumn stockminimo;
	}
}