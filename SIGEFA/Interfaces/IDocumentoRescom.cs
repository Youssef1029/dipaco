﻿using System;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IDocumentoRescom
    {
        Boolean InsertRescom(clsDocumentorescom rescom);
        Boolean InsertDetRescom(clsDetalleDocumentoRescom detrescom);
    }
}
