﻿using System;
using System.Data;
using SIGEFA.Entidades;


namespace SIGEFA.Interfaces
{
	interface IUsuario
    {
        Boolean Insert(clsUsuario UsuarioNuevo);
        Boolean Update(clsUsuario UsuarioNuevo);
        Boolean Delete(Int32 Codigo);

        Boolean Login(clsUsuario Usuario);
		Boolean LoginSeguridad(clsUsuario Usuario);

		clsUsuario CargaUsuario(Int32 Codigo);
		clsUsuario CargaUsuarioSinAdmin(Int32 Codigo);
		clsUsuario CargaUsuarioNivel();
        DataTable ListaUsuarios();
        DataTable BuscaUsuarios(Int32 Criterio, String Filtro);
        DataTable ListaCorreosUsuarios();

        DataTable correoTesoreria();
        DataTable ListaNiveles();

        DataTable ListaUsuarios_Empresa(Int32 Codigo);
		DataTable CargaUsuarios();
	}
}
