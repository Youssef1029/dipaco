﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IVehiculoTransporte
    {
        Boolean Insert(clsVehiculoTransporte NuevoVehiculoTransporte);
        Boolean Update(clsVehiculoTransporte VehiculoTransporte);
        Boolean Delete(Int32 Codigo);

        clsVehiculoTransporte CargaVehiculoTransporte(Int32 Codigo);
        DataTable ListaVehiculoTransportes();
        DataTable CargaVehiculoTransportes();

    }
}
