﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface ILinea
    {
        Boolean Insert(clsLinea NuevaLinea);
        Boolean Update(clsLinea Linea);
        Boolean Delete(Int32 Codigo);

        clsLinea CargaLinea(Int32 Codigo);
        DataTable ListaLineas(Int32 codFamilia);
    }
}
