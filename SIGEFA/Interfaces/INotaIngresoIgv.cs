﻿using SIGEFA.Entidades;
using System;

namespace SIGEFA.Interfaces
{
	interface INotaIngresoIgv
	{
		Boolean insert(clsNotaIngresoIgv notaIngresoIgv);
	}
}
