﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Interfaces
{
    interface IParametro
    {
        String obtenerParametroPorNombre(String nombreParametro);
    }
}
