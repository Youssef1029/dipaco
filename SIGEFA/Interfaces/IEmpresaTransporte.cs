﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IEmpresaTranporte
    {
        Boolean Insert(clsEmpresaTransporte NuevoEmpresaTranporte);
        Boolean Update(clsEmpresaTransporte EmpresaTranporte);
        Boolean Delete(Int32 Codigo);

        clsEmpresaTransporte CargaEmpresaTranporte(Int32 Codigo);
        clsEmpresaTransporte BuscaEmpresaTransporte(String RUC);
        DataTable CargaEmpresasTransporte();
        DataTable ListaEmpresaTranportes();
    }
}
