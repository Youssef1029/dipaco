﻿using System;
using System.Data;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
	interface IFamilias
    {
        Boolean Insert(clsFamilia NuevaFamilia);
        Boolean Update(clsFamilia Familia);
        Boolean Delete(Int32 Codigo);

        clsFamilia CargaFamilia(Int32 Codigo);
        DataTable ListaFamilias();
    }
}
