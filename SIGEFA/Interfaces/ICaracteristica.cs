﻿using System;
using System.Data;
using SIGEFA.Entidades;


namespace SIGEFA.Interfaces
{
	interface ICaracteristica
    {
        Boolean Insert(clsCaracteristica NuevaCaracteristica);
        Boolean Update(clsCaracteristica Caracteristica);
        Boolean Delete(Int32 Codigo);

        clsCaracteristica CargaCaracteristica(Int32 Codigo);
        DataTable ListaCaracteristicas();
    }
}
