﻿namespace SIGEFA.Reportes
{
    partial class frmListaFacturasCompras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvListaFacturasCompra = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvListaFacturasCompra
            // 
            this.crvListaFacturasCompra.ActiveViewIndex = -1;
            this.crvListaFacturasCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvListaFacturasCompra.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvListaFacturasCompra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvListaFacturasCompra.Location = new System.Drawing.Point(0, 0);
            this.crvListaFacturasCompra.Name = "crvListaFacturasCompra";
            this.crvListaFacturasCompra.Size = new System.Drawing.Size(447, 261);
            this.crvListaFacturasCompra.TabIndex = 0;
            this.crvListaFacturasCompra.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // frmListaFacturasCompras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 261);
            this.Controls.Add(this.crvListaFacturasCompra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmListaFacturasCompras";
            this.ShowIcon = false;
            this.Text = "Lista de Facturas de Compras";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvListaFacturasCompra;
    }
}