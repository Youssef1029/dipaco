﻿namespace SIGEFA.Reportes
{
    partial class frmRptVentxCliente2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvRptVentxCliente2 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvRptVentxCliente2
            // 
            this.crvRptVentxCliente2.ActiveViewIndex = -1;
            this.crvRptVentxCliente2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvRptVentxCliente2.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvRptVentxCliente2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvRptVentxCliente2.Location = new System.Drawing.Point(0, 0);
            this.crvRptVentxCliente2.Name = "crvRptVentxCliente2";
            this.crvRptVentxCliente2.SelectionFormula = "";
            this.crvRptVentxCliente2.Size = new System.Drawing.Size(635, 370);
            this.crvRptVentxCliente2.TabIndex = 1;
            this.crvRptVentxCliente2.ViewTimeSelectionFormula = "";
            // 
            // frmRptVentxCliente2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 370);
            this.Controls.Add(this.crvRptVentxCliente2);
            this.Name = "frmRptVentxCliente2";
            this.Text = "Reporte de Venta por Cliente / Articulo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvRptVentxCliente2;

    }
}