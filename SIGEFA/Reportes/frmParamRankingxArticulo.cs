﻿using System;
using SIGEFA.Formularios;
using SIGEFA.Reportes.clsReportes;

namespace SIGEFA.Reportes
{
	public partial class frmParamRankingxArticulo : DevComponents.DotNetBar.Office2007Form
    {
        clsReporteRankingxArticulo ds = new clsReporteRankingxArticulo();

        public frmParamRankingxArticulo()
        {
            InitializeComponent();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            CRRankingArticulo rpt = new CRRankingArticulo();
            frmRptRankingArticulo frm = new frmRptRankingArticulo();
            rpt.SetDataSource(ds.Reporte_Ranking(dtpFecha1.Value, dtpFecha2.Value, frmLogin.iCodAlmacen).Tables[0]);
            frm.crvRankingArticulo.ReportSource = rpt;
            frm.Show();
        }
    }
}
