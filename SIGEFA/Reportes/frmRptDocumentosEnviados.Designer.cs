﻿namespace SIGEFA.Reportes
{
	partial class frmRptDocumentosEnviados
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRptDocumentosEnviados));
            this.crvRptDocumentosEnviados = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvRptDocumentosEnviados
            // 
            this.crvRptDocumentosEnviados.ActiveViewIndex = -1;
            this.crvRptDocumentosEnviados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvRptDocumentosEnviados.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvRptDocumentosEnviados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvRptDocumentosEnviados.Location = new System.Drawing.Point(0, 0);
            this.crvRptDocumentosEnviados.Name = "crvRptDocumentosEnviados";
            this.crvRptDocumentosEnviados.Size = new System.Drawing.Size(361, 261);
            this.crvRptDocumentosEnviados.TabIndex = 0;
            this.crvRptDocumentosEnviados.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // frmRptDocumentosEnviados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 261);
            this.Controls.Add(this.crvRptDocumentosEnviados);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRptDocumentosEnviados";
            this.Text = "Reporte Documentos Electrónicos Enviados";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

		}

		#endregion

		public CrystalDecisions.Windows.Forms.CrystalReportViewer crvRptDocumentosEnviados;
	}
}