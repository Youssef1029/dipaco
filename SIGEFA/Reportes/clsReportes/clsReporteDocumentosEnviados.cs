﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using System;
using System.Data;

namespace SIGEFA.Reportes.clsReportes
{
	class clsReporteDocumentosEnviados
	{
		clsConexionMysql con = new clsConexionMysql();
		MySqlCommand cmd = null;
		MySqlDataReader dr = null;
		MySqlDataAdapter adap = null;
		DataSet set = null;

		public DataSet DocumentosEnviados(Int32 codAlmacen, DateTime fechaInicio, DateTime fechaFin, Int32 codDocumento)
		{
			try
			{
				set = new DataSet();
				con.conectarBD();
				cmd = new MySqlCommand("ReporteDocumentosElectronicosEnviados", con.conector);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandTimeout = 15;
				cmd.Parameters.AddWithValue("codigo_almacen", codAlmacen);
				cmd.Parameters.AddWithValue("fecha_inicio", fechaInicio);
				cmd.Parameters.AddWithValue("fecha_fin", fechaFin);
				cmd.Parameters.AddWithValue("codigo_documento", codDocumento);
				adap = new MySqlDataAdapter(cmd);
				adap.Fill(set);
				set.WriteXml("C:\\XML\\DocumentosEnviadosRPT.xml", XmlWriteMode.WriteSchema);
				return set;
			}
			catch (MySqlException ex)
			{
				throw ex;
			}
			finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
		}
	}
}
