﻿using System;
using System.Windows.Forms;
using SIGEFA.Formularios;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.Reportes.clsReportes;

namespace SIGEFA.Reportes
{
	public partial class frmParamVentxCliente2 : DevComponents.DotNetBar.Office2007Form
    {


        clsAdmEmpresa admEmp = new clsAdmEmpresa();
        clsAdmFormaPago AdmPago = new clsAdmFormaPago();
        clsReporteVentxCliente ds = new clsReporteVentxCliente();
        public clsCliente cli = new clsCliente();
        clsAdmCliente AdmCli = new clsAdmCliente();
        clsProducto pro = new clsProducto();
        clsAdmProducto AdmPro = new clsAdmProducto();
        clsAdmMoneda AdmMon = new clsAdmMoneda();
        private Int32 Tipo = 0;



        public frmParamVentxCliente2()
        {
            InitializeComponent();
        }

        private void frmParamVentxCliente2_Load(object sender, EventArgs e)
        {
        
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {

            CRVentxCliente2 rpt = new CRVentxCliente2();
            frmRptVentxCliente frm = new frmRptVentxCliente();

            rpt.SetDataSource(ds.ReporteVentasxCliente(frmLogin.iCodAlmacen, dtpFecha1.Value, dtpFecha2.Value,
                    rbTodosCli.Checked, rbTodosArt.Checked,
                    txtUnArt.Text, txtUnCli.Text).Tables[0]);

            frm.crvRptVentxCliente.ReportSource = rpt;
            frm.Show();
        }

        private void frmParamVentxCliente2_Load_1(object sender, EventArgs e)
        {
            dtpFecha1.Value = DateTime.Now;
            dtpFecha2.Value = DateTime.Now;

        }

        private void rbCli_CheckedChanged(object sender, EventArgs e)
        {
            txtUnCli.Text = "";
            txtCliente.Text = "";
            txtUnCli.Enabled = rbCli.Checked;
            txtUnCli.Focus();
        }

        private void rbArt_CheckedChanged(object sender, EventArgs e)
        {
            txtUnArt.Text = "";
            txtArticulo.Text = "";
            txtUnArt.Enabled = rbArt.Checked;
            txtUnArt.Focus();
        }


        private void CargaProducto(Int32 Codigo)
        {
            pro = AdmPro.CargaProducto(Codigo, frmLogin.iCodAlmacen);
            txtUnArt.Text = pro.Referencia;
            txtArticulo.Text = pro.Descripcion;
            txtCodProd.Text = pro.CodProducto.ToString();
        }

        private void CargaCliente(Int32 Codigo)
        {
            cli = AdmCli.MuestraCliente(Codigo);
            txtUnCli.Text = cli.RucDni;
            txtCodCli.Text = cli.CodCliente.ToString();
            txtCliente.Text = cli.RazonSocial;
        }


        private Boolean BuscaCliente()
        {
            cli = AdmCli.BuscaCliente(txtUnCli.Text, Tipo);
            if (cli != null)
            {
                txtUnCli.Text = cli.RucDni;
                txtCliente.Text = cli.RazonSocial;
                txtCodCli.Text = cli.CodCliente.ToString();
                return true;
            }
            else
            {
                txtUnCli.Text = "";
                txtCliente.Text = "";
                txtCodCli.Text = "";
                return false;
            }
        }


        private Boolean BuscaProducto()
        {
            pro = AdmPro.CargaProductoDetalleR(txtUnArt.Text, frmLogin.iCodAlmacen, 1, 0);
            if (pro != null)
            {
                txtCodProd.Text = pro.CodProducto.ToString();
                txtUnArt.Text = pro.Referencia;
                txtArticulo.Text = pro.Descripcion;
                return true;
            }
            else
            {
                txtCodProd.Text = "";
                txtUnArt.Text = "";
                txtArticulo.Text = "";
                return false;
            }
        }

        private void txtUnArt_TextChanged(object sender, EventArgs e)
        {
            txtCodProd.Text = "";
            txtArticulo.Text = "";
        }

        private void txtUnArt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtUnArt.Text != "")
                {
                    if (BuscaProducto())
                    {
                        ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("El producto no existe, Presione F1 para consultar la tabla de ayuda", "DETALLE DE ARTICULO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void txtUnArt_KeyDown(object sender, KeyEventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            if (rbArt.Checked)
            {
                if (e.KeyCode == Keys.F1)
                {
                    frmProductosLista frm = new frmProductosLista();
                    frm.Procede = 15; //(8) Procede desde el formulario frmParamVentxVendedor
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        if (frm.pro.CodProducto != 0)
                            CargaProducto(frm.pro.CodProducto);
                    
                    }
                }
            }

            this.Cursor = Cursors.Default;

        }

        private void txtUnCli_TextChanged(object sender, EventArgs e)
        {
            txtCliente.Text = "";
            txtCodCli.Text = "";
        }

        private void txtUnCli_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtUnCli.Text != "")
                {
                    if (BuscaCliente())
                    {
                        ProcessTabKey(true);

                    }
                    else
                    {
                        MessageBox.Show("El Cliente no existe, Presione F1 para consultar la tabla de ayuda", "Facturacion Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void txtUnCli_KeyDown(object sender, KeyEventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            if (rbCli.Checked)
            {
                if (e.KeyCode == Keys.F1)
                {
                    frmClientesLista frm = new frmClientesLista();
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        CargaCliente(frm.GetCodigoCliente());
                        this.Cursor = Cursors.Default;
                    }
                    else txtUnCli.Focus();
                }
            }

            this.Cursor = Cursors.Default;
        }

		private void btnCancelar_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
